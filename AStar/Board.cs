﻿using UnityEngine;

namespace YOZH.Plugins.AStar
{
	public class Board : MonoBehaviour
	{
		private static Board instance = null;
		public static Board Instance {
			get {
				if (!instance) instance = FindObjectOfType<Board>();
				if (!instance) instance = new GameObject(typeof(Board).Name, typeof(Board)).GetComponent<Board>();

				return instance;
			}
		}

		[SerializeField]
		private Vector3 cellSize = Vector3.one;

		private GridGraph grid = null;
		public GridGraph Grid {
			get {
				if (grid == null || grid.Size == 0) {
					grid = createGrid();
				}
				return grid;
			}
		}

		protected virtual GridGraph createGrid()
		{
			return new GridGraph(cellSize, new Bounds(transform.position, transform.localScale));
		}

#if UNITY_EDITOR
		private void OnDrawGizmos()
		{
			GridGraph debugGrid = (Application.isPlaying) ? Grid : createGrid();

			int maxCost = debugGrid.MaxNodeCost;

			for (int xIndex = 0; xIndex < debugGrid.XSize; xIndex++) {
				for (int yIndex = 0; yIndex < debugGrid.YSize; yIndex++) {
					for (int zIndex = 0; zIndex < debugGrid.ZSize; zIndex++) {
						Node node = debugGrid.GetNode(xIndex, yIndex, zIndex);

						if (Application.isPlaying) {
							if (node.IsWalkable) {
								Gizmos.color = new Color(1, 0, 0, ((float)node.CostFactor / maxCost) * 0.5f);
								UnityEditor.Handles.Label(node.Bounds.center, node.CostFactor.ToString());
							}
							else {
								Gizmos.color = new Color(1, 0, 1, 0.7f);
								UnityEditor.Handles.Label(node.Bounds.center, "Not walkable");
							}
							Gizmos.DrawCube(node.Bounds.center, node.Bounds.size);
							Gizmos.color = Color.red;
							Gizmos.DrawWireCube(node.Bounds.center, node.Bounds.size);
						}
						else {
							Gizmos.color = new Color(1, 1, 1, 0.2f);
							Gizmos.DrawWireCube(node.Bounds.center, node.Bounds.size);
						}
					}
				}
			}

			Gizmos.color = new Color(1, 1, 1, 0.3f);
			Gizmos.DrawWireCube(transform.position, debugGrid.Bounds.size);
		}
#endif

	}
}