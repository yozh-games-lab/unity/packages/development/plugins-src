﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace YOZH.Plugins.AStar
{
	public class NavigationAgent : MonoBehaviour
	{
		private Stack<Vector3> calculatedPath = new Stack<Vector3>();
		private Node cachedStartNode = null;
		private Node cachedFinishNode = null;
		private PointsCollection points = new PointsCollection(32);

		private void Awake()
		{
			Node.OnUpdated += onNodeUpdated;
		}
		private void OnDestroy()
		{
			Node.OnUpdated -= onNodeUpdated;
		}
#if UNITY_EDITOR
		private void OnDrawGizmos()
		{

			Gizmos.color = new Color(0, 1, 0, 1);

			Vector3 prevPoint = transform.position;
			Vector3[] drawnPath = new Vector3[calculatedPath.Count];
			calculatedPath.CopyTo(drawnPath, 0);
			for (int index = 0; index < drawnPath.Length; index++) {
				Vector3 point = drawnPath[index];

				Gizmos.DrawSphere(point, 0.3f);
				Gizmos.DrawLine(prevPoint, point);

				prevPoint = point;
			}

		}
#endif

		public Stack<Vector3> GetPath(Vector3 start, Vector3 finish, float viewRadius = 0)
		{
			Node startNode = Board.Instance.Grid.GetNode(start);
			Node finishNode = Board.Instance.Grid.GetNode(finish);
			if (isCalculatedPathActual(startNode, finishNode)) {
				return calculatedPath;
			}

			calculatedPath.Clear();
			calculatedPath.Push(finish);
			executeAStar(startNode, finishNode, viewRadius, calculatedPath);

			cachedStartNode = startNode;
			cachedFinishNode = finishNode;

			return calculatedPath;
		}

		private void executeAStar(Node fromNode, Node toNode, float viewRadius, Stack<Vector3> path)
		{
			points.Clear();
			int currentIndex = points.Add();
			points.SetNode(currentIndex, fromNode);
			points.SetInspectedCost(currentIndex, Vector3.Distance(fromNode.Bounds.center, toNode.Bounds.center));
			points.SetHeuristicCost(currentIndex, 0);

			while (points.GetCheapestIndex(ref currentIndex)) {
				Node currentNode = points.GetNode(currentIndex);
				points.SetIsClosed(currentIndex, true);

				if (currentNode == toNode) {
					restorePath(currentIndex, path);
					return;
				}

				for (int neighbourIndex = 0; neighbourIndex < currentNode.NeighbourCount; neighbourIndex++) {
					Node neighbourNode = currentNode.GetNeighbour(neighbourIndex);

					int neighbourPointIndex = points.GetIndexOf(item => item.Node == neighbourNode);

					if (!neighbourNode.IsWalkable ||
						(neighbourPointIndex >= 0 && points.GetIsClosed(neighbourPointIndex))) {
						continue;
					}

					if (neighbourPointIndex < 0) neighbourPointIndex = points.Add();
					points.SetPrevious(neighbourPointIndex, currentIndex);
					points.SetNode(neighbourPointIndex, neighbourNode);

					points.SetInspectedCost(neighbourPointIndex, neighbourNode.GetHeuristicCost(currentNode) + points.GetInspectedCost(currentIndex));
					//float radius = Vector3.Distance(fromNode.Bounds.center, neighbourNode.Bounds.center);
					//if (radius >= viewRadius || neighbourNode.CostFactor != currentNode.CostFactor) {
					points.SetHeuristicCost(neighbourPointIndex, neighbourNode.GetHeuristicCost(toNode));
					//}
					//else {
					//	float hCost = points.GetHeuristicCost(neighbourIndex);
					//	points.SetHeuristicCost(neighbourPointIndex, Mathf.Min(hCost, points.GetInspectedCost(neighbourIndex)));
					//}
				}
			}
		}

		private void restorePath(int index, Stack<Vector3> path)
		{
			while (points.GetPrevious(index) >= 0) {
				path.Push(points.GetNode(index).Bounds.center);
				index = points.GetPrevious(index);
			}
		}

		private void onNodeUpdated(Node sender)
		{
			if (calculatedPath.Contains(sender.Bounds.center)) {
				calculatedPath.Clear();
			}
		}

		private bool isCalculatedPathActual(Node currentStartNode, Node currentFinishNode)
		{
			return currentStartNode == cachedStartNode && currentFinishNode == cachedFinishNode && calculatedPath.Count > 0;
		}

		private struct Point
		{
			public bool IsClosed;
			public int Previous;
			public Node Node;
			public float InspectedCost;
			public float HeuristicCost;
			public float EstimatedCost { get { return InspectedCost + HeuristicCost; } }

			public void Reset()
			{
				IsClosed = false;
				Previous = -1;
				Node = null;
				InspectedCost = 0;
				HeuristicCost = float.MaxValue;
			}
		}

		private class PointsCollection
		{
			private Point[] items;
			public int Count { get; private set; }

			public PointsCollection(int capacity)
			{
				items = new Point[capacity];
				Clear();
			}

			public bool GetIsClosed(int index) { return items[clampIndex(index)].IsClosed; }
			public void SetIsClosed(int index, bool value) { items[clampIndex(index)].IsClosed = value; }

			public int GetPrevious(int index) { return items[clampIndex(index)].Previous; }
			public void SetPrevious(int index, int value) { items[clampIndex(index)].Previous = value; }

			public Node GetNode(int index) { return items[clampIndex(index)].Node; }
			public void SetNode(int index, Node value) { items[clampIndex(index)].Node = value; }

			public float GetInspectedCost(int index) { return items[clampIndex(index)].InspectedCost; }
			public void SetInspectedCost(int index, float value) { items[clampIndex(index)].InspectedCost = value; }

			public float GetHeuristicCost(int index) { return items[clampIndex(index)].HeuristicCost; }
			public void SetHeuristicCost(int index, float value) { items[clampIndex(index)].HeuristicCost = value; }

			public float GetEstimatedCost(int index) { return items[clampIndex(index)].EstimatedCost; }

			public int GetIndexOf(Predicate<Point> condition)
			{
				for (int index = 0; index < Count; index++) {
					if (condition(items[index])) {
						return index;
					}
				}
				return -1;
			}

			public void Clear()
			{
				Count = 0;
				items[Count].Reset();
			}

			public int Add()
			{
				if (Count >= items.Length) {
					increaseCapacity();
				}

				items[Count].Reset();
				return Count++;
			}

			public bool GetCheapestIndex(ref int current)
			{
				float cost = float.MaxValue;
				current = -1;

				for (int index = 0; index < Count; index++) {
					if (!items[index].IsClosed && items[index].EstimatedCost <= cost) {
						cost = items[index].EstimatedCost;
						current = index;
					}
				}

				return current >= 0;
			}

			private void increaseCapacity()
			{
				Point[] newItems = new Point[Count * 2];
				for (int index = 0; index < items.Length; index++) {
					newItems[index] = items[index];
				}
				items = newItems;
			}

			private int clampIndex(int index)
			{
				return Mathf.Clamp(index, 0, Count - 1);
			}
		}
	}
}