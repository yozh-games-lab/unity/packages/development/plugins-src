﻿using UnityEngine;
using System.Collections.Generic;

namespace YOZH.Plugins.AStar
{
	public class ObstacleAgent : MonoBehaviour
	{
		public const int MAX_WEIGHT = short.MaxValue;

		[SerializeField]
		private bool isWalkable = true;
		public bool IsWalkable { get { return isWalkable; } }

		[SerializeField]
		private short weight = MAX_WEIGHT;
		public int Weight { get { return weight; } }

		[SerializeField]
		private bool centerOnly = false;

		protected List<Node> intersectedNodes = new List<Node>();
		private Node lastBoardPosition = null;
		protected virtual Bounds thisBounds { get { return new Bounds(); } }

		public void RegisterOnBoard()
		{
			intersectedNodes = Board.Instance.Grid.GetIntersectedNodes(thisBounds, centerOnly);

			foreach (Node node in intersectedNodes) {
				node.AddObstacle(this);
			}
		}

		public void UnregisterFromBoard()
		{
			foreach (Node node in intersectedNodes) {
				node.RemoveObstacle(this);
			}

			intersectedNodes.Clear();
		}

		public void UpdateBoardRegistration()
		{

			Node currentBoardPosition = Board.Instance.Grid.GetNode(thisBounds.center);
			if (currentBoardPosition == lastBoardPosition) return;

			lastBoardPosition = currentBoardPosition;
			List<Node> currentIntersectedNodes = Board.Instance.Grid.GetIntersectedNodes(thisBounds, centerOnly);

			foreach (Node node in currentIntersectedNodes) {
				if (!intersectedNodes.Contains(node)) {
					node.AddObstacle(this);
				}

				intersectedNodes.Remove(node);
			}

			foreach (Node node in intersectedNodes) {
				node.RemoveObstacle(this);
			}

			intersectedNodes = currentIntersectedNodes;
		}
	}
}