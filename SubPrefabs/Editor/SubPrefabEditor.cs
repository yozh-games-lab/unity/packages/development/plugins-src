﻿using UnityEngine;
using UnityEditor;
using YOZH.Core.APIExtentions.Unity;
using UnityEngine.SceneManagement;
using System.IO;

namespace YOZH.Plugins.SubPrefabs.Editor
{
	[CustomEditor(typeof(SubPrefab))]
	public class SubPrefabEditor : UnityEditor.Editor
	{
		private const string PROGRESS_BAR_TITLE = "Updating subprefabs...";
		private static string progressBarMessage = "";
		private static float progressBarProgress = 0;

		[MenuItem("HBG/Dev tools/Update subprefabs")]
		public static void UpdateSubPrefabs()
		{

			GameObject[] targets = SceneManager.GetActiveScene().GetRootGameObjects();
			for (int index = 0; index < targets.Length; index++) {
				GameObject target = targets[index];
				updateObjectRecursive(target);
			}
			EditorUtility.ClearProgressBar();
		}

		public override void OnInspectorGUI()
		{
			GUI.enabled = false;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"), true, new GUILayoutOption[0]);
			DrawPropertiesExcluding(serializedObject, "m_Script");
			GUI.enabled = true;

			SerializedProperty guid = serializedObject.FindProperty("guid");
			Object targetObj = (Component)PrefabUtility.GetPrefabParent(target);
			if (targetObj != null) {
				if (targetObj == target) {
					guid.stringValue = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(targetObj));
					serializedObject.ApplyModifiedPropertiesWithoutUndo();
				}
			}
			else {
				targetObj = PrefabUtility.GetPrefabObject(target);
				if (targetObj != null) {
					guid.stringValue = AssetDatabase.AssetPathToGUID(AssetDatabase.GetAssetPath(targetObj));
					serializedObject.ApplyModifiedPropertiesWithoutUndo();
				}
			}

		}

		private static void updateObjectRecursive(GameObject target)
		{
			if (target.transform.childCount == 0) return;

			progressBarMessage = Path.Combine(progressBarMessage, target.name);

			for (int index = 0; index < target.transform.childCount; index++) {
				progressBarProgress = (float)index / target.transform.childCount;

				GameObject child = target.transform.GetChild(index).gameObject;
				child = updateObject(child);
				updateObjectRecursive(child);
			}
			progressBarMessage = Path.GetDirectoryName(progressBarMessage);
		}

		private static GameObject updateObject(GameObject target)
		{
			SubPrefab targetComponent = target.GetComponent<SubPrefab>();
			if (targetComponent && !string.IsNullOrEmpty(targetComponent.GUID)) {
				GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(AssetDatabase.GUIDToAssetPath(targetComponent.GUID));
				if (prefab != null) {
					EditorUtility.DisplayProgressBar(PROGRESS_BAR_TITLE, Path.Combine(progressBarMessage, target.name), progressBarProgress);

					SceneData data = new SceneData(target);

					PrefabUtility.DisconnectPrefabInstance(target);
					target = PrefabUtility.ConnectGameObjectToPrefab(targetComponent.gameObject, prefab);

					data.Apply(target);
				}
			}
			return target;
		}

		private class SceneData
		{
			private Vector3 position = Vector3.zero;
			private Quaternion rotation = Quaternion.identity;
			private Vector3 localScale = Vector3.zero;

			private bool activeSelf = false;

			public SceneData(GameObject original)
			{
				position = original.transform.position;
				rotation = original.transform.rotation;
				localScale = original.transform.localScale;

				activeSelf = original.activeSelf;
			}

			public void Apply(GameObject target)
			{
				target.transform.TransferTo(position, rotation, localScale);
				target.SetActive(activeSelf);
			}
		}
	}
}