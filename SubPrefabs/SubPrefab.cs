﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace YOZH.Plugins.SubPrefabs
{
	public class SubPrefab : MonoBehaviour
	{
		[SerializeField]
		private string guid = "";
		public string GUID { get { return guid; } }
	}
}