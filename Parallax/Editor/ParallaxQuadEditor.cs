﻿using UnityEditor;

namespace YOZH.Plugins.Parallax.Editor
{
	[CustomEditor(typeof(ParallaxQuad)), CanEditMultipleObjects]
	public class ParallaxQuadEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			ParallaxQuad src = (ParallaxQuad)target;
			if (src.CurrentTranslateMode == ParallaxQuad.TranslateMode.Scale || src.CurrentTranslateMode == ParallaxQuad.TranslateMode.OffsetAndScale) {
				src.CurrentScaleMode = (ParallaxQuad.ScaleMode)EditorGUILayout.EnumPopup("Scale Mode", src.CurrentScaleMode);
			}

			if (src.CurrentTranslateMode == ParallaxQuad.TranslateMode.OffsetAndScale) {
				src.ScaleSpeedFactor = EditorGUILayout.FloatField("Scale Speed Factor", src.ScaleSpeedFactor);
				src.OffsetSpeedFactor = EditorGUILayout.FloatField("Offset Speed Factor", src.OffsetSpeedFactor);
			}
			else {
				src.ScaleSpeedFactor = 1;
				src.OffsetSpeedFactor = 1;
			}
		}
	}
}