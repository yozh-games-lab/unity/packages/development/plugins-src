﻿using UnityEngine;

namespace YOZH.Plugins.Parallax
{
	public abstract class ParallaxLayer : ParallaxGroup
	{
		public float SpeedFactor;

		public sealed override void Translate(Vector2 delta)
		{
			TranslateThis(delta * (int)translateDirection);
			foreach (ParallaxLayer layer in layers) {
				if (translateInactive || layer.gameObject.activeSelf) TranslateChildren(layer, delta * (int)translateDirection);
			}
		}

		protected abstract void TranslateThis(Vector2 delta);
		protected abstract void TranslateChildren(ParallaxLayer layer, Vector2 delta);
	}
}