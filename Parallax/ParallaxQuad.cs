﻿using UnityEngine;

namespace YOZH.Plugins.Parallax
{
	[RequireComponent(typeof(MeshRenderer))]
	[RequireComponent(typeof(MeshFilter))]
	public class ParallaxQuad : ParallaxLayer
	{
		private Vector2 ppuFactor = Vector2.one;
		public float PixelsPerUnit = 100;

		public enum TranslateMode { Offset, Scale, OffsetAndScale }
		public TranslateMode CurrentTranslateMode = TranslateMode.Offset;

		public enum ScaleMode { ProportionalX, ProportionalY, Free }
		[HideInInspector]
		public ScaleMode CurrentScaleMode = ScaleMode.Free;

		[HideInInspector]
		public float OffsetSpeedFactor = 1;
		[HideInInspector]
		public float ScaleSpeedFactor = 1;

		private Vector2 translateThisDelta = Vector2.zero;

		void Start()
		{
			ppuFactor = new Vector2(PixelsPerUnit / transform.lossyScale.x / 100, PixelsPerUnit / transform.lossyScale.y / 100);
		}

		protected override void TranslateThis(Vector2 delta)
		{
			if (CurrentTranslateMode == TranslateMode.Offset || CurrentTranslateMode == TranslateMode.OffsetAndScale) {
				translateThisDelta = delta;

				GetComponent<Renderer>().material.SetTextureOffset("_MainTex", GetComponent<Renderer>().material.mainTextureOffset - new Vector2(delta.x * SpeedFactor * OffsetSpeedFactor * ppuFactor.x, delta.y * SpeedFactor * OffsetSpeedFactor * ppuFactor.y));
			}
			if (CurrentTranslateMode == TranslateMode.Scale || CurrentTranslateMode == TranslateMode.OffsetAndScale) {
				Vector2 newScale = Vector2.zero;
				switch (CurrentScaleMode) {
					case ScaleMode.Free:
						newScale = GetComponent<Renderer>().material.mainTextureScale + new Vector2(delta.x * SpeedFactor * ScaleSpeedFactor * ppuFactor.x,
							delta.y * SpeedFactor * ScaleSpeedFactor * ppuFactor.y);
						break;
					case ScaleMode.ProportionalX:
						newScale = GetComponent<Renderer>().material.mainTextureScale + new Vector2(delta.x * SpeedFactor * ScaleSpeedFactor * ppuFactor.x,
							delta.x * SpeedFactor * ScaleSpeedFactor * ppuFactor.x);
						break;
					case ScaleMode.ProportionalY:
						newScale = GetComponent<Renderer>().material.mainTextureScale + new Vector2(delta.y * SpeedFactor * ScaleSpeedFactor * ppuFactor.y,
							delta.y * SpeedFactor * ScaleSpeedFactor * ppuFactor.y);
						break;
				}

				GetComponent<Renderer>().material.SetTextureScale("_MainTex", newScale);
			}
		}

		protected override void TranslateChildren(ParallaxLayer layer, Vector2 delta)
		{
			Vector2 d = delta;
			d.x = d.x * transform.localScale.x;
			d.y = d.y * transform.localScale.y;
			layer.Translate(d + translateThisDelta);
		}
	}
}