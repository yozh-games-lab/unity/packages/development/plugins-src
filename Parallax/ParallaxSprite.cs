﻿using UnityEngine;

namespace YOZH.Plugins.Parallax
{
	public class ParallaxSprite : ParallaxLayer
	{
		public enum TranslateMode
		{
			Position,
			Scale,
			PositionAndScale
		}
		public TranslateMode CurrentTranslateMode = TranslateMode.Position;

		public enum ScaleMode
		{
			ProportionalX,
			ProportionalY,
			Free
		}
		[HideInInspector]
		public ScaleMode CurrentScaleMode = ScaleMode.Free;

		[HideInInspector]
		public float PositionSpeedFactor = 1;
		[HideInInspector]
		public float ScaleSpeedFactor = 1;

		protected override void TranslateThis(Vector2 delta)
		{
			if (CurrentTranslateMode == TranslateMode.Position || CurrentTranslateMode == TranslateMode.PositionAndScale) {
				transform.Translate(new Vector2(delta.x * SpeedFactor * PositionSpeedFactor * transform.lossyScale.x,
					delta.y * SpeedFactor * PositionSpeedFactor * transform.lossyScale.y));
			}
			if (CurrentTranslateMode == TranslateMode.Scale || CurrentTranslateMode == TranslateMode.PositionAndScale) {
				Vector3 newScale = transform.localScale;

				switch (CurrentScaleMode) {
					case ScaleMode.Free:
						newScale.x += delta.x * SpeedFactor * ScaleSpeedFactor;
						newScale.y += delta.y * SpeedFactor * ScaleSpeedFactor;
						break;
					case ScaleMode.ProportionalX:
						newScale.x += delta.x * SpeedFactor * ScaleSpeedFactor;
						newScale.y += delta.x * SpeedFactor * ScaleSpeedFactor;
						break;
					case ScaleMode.ProportionalY:
						newScale.x += delta.y * SpeedFactor * ScaleSpeedFactor;
						newScale.y += delta.y * SpeedFactor * ScaleSpeedFactor;
						break;
				}

				if (newScale.x > 0 && newScale.y > 0) {
					transform.localScale = newScale;
				}
			}
		}

		protected override void TranslateChildren(ParallaxLayer layer, Vector2 delta)
		{
			layer.Translate(delta);
		}
	}
}