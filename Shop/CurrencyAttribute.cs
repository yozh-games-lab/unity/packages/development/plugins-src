﻿using YOZH.Core.CustomAttributes;

namespace YOZH.Plugins.Shop
{
	public class CurrencyAttribute : PopupAttribute
	{
		public CurrencyAttribute()
		{
			Choices = Shop.Instance.Currencies;
		}
	}
}