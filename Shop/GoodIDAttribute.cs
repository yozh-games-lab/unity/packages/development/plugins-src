﻿using YOZH.Core.CustomAttributes;

namespace YOZH.Plugins.Shop
{
	public class GoodIDAttribute : PopupAttribute
	{
		public GoodIDAttribute()
		{
			Choices = ShopStore.Instance.GetAvailableItemIDs();
		}
	}
}