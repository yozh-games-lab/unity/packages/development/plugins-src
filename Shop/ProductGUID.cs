﻿using UnityEngine;
using YOZH.Core.GUIDLinks;
using System;

namespace YOZH.Plugins.Shop
{
	[Serializable]
	public class ProductGUID : GUIDLink<IProduct>
	{
		protected override IProduct getInstance()
		{
			UnityEngine.Object obj = Resources.Load(loadPath);
			return (obj is GameObject) ? ((GameObject)obj).GetComponent<IProduct>() : obj as IProduct;
		}
	}
}