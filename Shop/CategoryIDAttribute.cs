﻿using YOZH.Core.CustomAttributes;

namespace YOZH.Plugins.Shop
{
	public class CategoryIDAttribute : PopupAttribute
	{
		public CategoryIDAttribute()
		{
			Choices = Shop.Instance.Categories;
		}
	}
}