﻿using YOZH.Plugins.UIExt;


namespace YOZH.Plugins.Shop
{
	public class GoodInfo : IGood
	{
		public GoodInfo(IGood good)
		{
			Count = good.Count;
			UIPresentation = good.UIPresentation;
			ID = good.ID;
		}

		#region IGood implementation
		public uint Count { get; private set; }
		public UIPresentation UIPresentation { get; private set; }
		public string ID { get; private set; }

		public void Validate(GoodValidationCallbak callback)
		{
			callback.Invoke(true);
		}
		#endregion

		#region IDisposable implementation
		public void Dispose()
		{
			// Do noting
		}
		#endregion

		#region ICloneable implementation
		public object Clone()
		{
			return new GoodInfo(this);
		}
		public object Clone(uint count)
		{
			return Clone();
		}
		#endregion
	}
}
