﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Shop
{
	public class ShopStore : ScriptableObject
	{
		public const string RESOURCE_PATH = "GameSettings/Shop/ShopStore";

		private static ShopStore instance = null;
		public static ShopStore Instance {
			get {
				if (!instance) {
					instance = Resources.Load<ShopStore>(RESOURCE_PATH);
				}
				return instance;
			}
		}
		[SerializeField]
		private List<GoodGUID> items = new List<GoodGUID>();

		public IGood GetItem(string goodID, uint count)
		{
			if (String.IsNullOrEmpty(goodID)) throw new ArgumentNullException("goodID");

			IGood result = items.Find(item => item.ID == goodID).Instance;
			if (Application.isPlaying) {
				return result.Clone(count) as IGood;
			}
			else {
				return result;
			}
		}
		public IGood GetItemInfo(string goodID)
		{
			if (String.IsNullOrEmpty(goodID)) throw new ArgumentNullException("goodID");

			return new GoodInfo(items.Find(item => item.ID == goodID).Instance);
		}
		public bool TryGetItem(string goodID, uint count, out IGood result)
		{
			result = GetItem(goodID, count);
			return result != null;
		}
		public bool TryGetItem<T>(string goodID, uint count, out T result) where T : IGood
		{
			result = (T)GetItem(goodID, count);
			return result != null;
		}
		public string[] GetAvailableItemIDs()
		{
			List<string> result = new List<string>();
			foreach (GoodGUID item in items) {
				result.Add(item.ID);
			}
			return result.ToArray();
		}

	}
}