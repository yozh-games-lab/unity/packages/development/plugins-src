﻿namespace YOZH.Plugins.Shop
{
	public interface IShopCustomer
	{
		void Give(IGood good);
		bool TakeAway(string goodID, uint count, out IGood[] result);

		uint GetBalance(string goodID);

		event ShopCustomerEvent OnBalanceChanged;
	}

	public delegate void ShopCustomerEvent(IShopCustomer customer);
}