﻿using UnityEngine;
using YOZH.Core.CustomAttributes;
using YOZH.Plugins.UIExt;

namespace YOZH.Plugins.Shop
{
	public class DefaultGood : MonoBehaviour, IGood
	{
		[SerializeField, UniqueID(4, "1234567890", "Good_")]
		private string id = "";

		[SerializeField]
		private uint count = 1;

		[SerializeField]
		private UIPresentation uiPresentation = new UIPresentation();


		#region IGood implementation
		public uint Count { get { return count; } }
		public string ID { get { return id; } }
		public UIPresentation UIPresentation { get { return uiPresentation; } }

		public void Validate(GoodValidationCallbak callback)
		{
			callback.Invoke(true);
		}

		public object Clone()
		{
			return Clone(1);
		}

		public object Clone(uint count)
		{
			var clone = Instantiate(this);
			clone.count *= count;
			return clone;
		}

		public void Dispose()
		{
			Destroy(gameObject);
		}
		#endregion
	}
}