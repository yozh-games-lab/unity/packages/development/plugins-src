﻿using System;
using UnityEngine;

namespace YOZH.Plugins.Shop
{
	[Serializable]
	public class ExchangeRate
	{
		[SerializeField, Currency]
		private string currencyID;
		public string CurrencyId {
			get { return currencyID; }
		}

		[SerializeField]
		private uint price;
		public uint Price {
			get { return price; }
			set { price = value; }
		}

		public ExchangeRate(string currencyId, uint price)
		{
			this.currencyID = currencyId;
			this.price = price;
		}

		public override string ToString()
		{
			return String.Format("{0} - {1}", currencyID, price);
		}

	}
}
