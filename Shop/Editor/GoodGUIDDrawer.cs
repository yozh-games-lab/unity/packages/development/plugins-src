﻿using UnityEngine;
using UnityEditor;
using YOZH.Core.GUIDLinks.Editor;
using YOZH.Core.APIExtentions.Unity;

namespace YOZH.Plugins.Shop.Editor
{
	[CustomPropertyDrawer(typeof(GoodGUID))]
	public class GoodGUIDDrawer : GUIDLinkPropertyDrawer<IGood>
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			base.OnGUI(position, property, label);

			SerializedProperty goodID = property.FindPropertyRelative("id");
			SerializedProperty assetPath = property.FindPropertyRelative(ASSET_PATH);

			IGood targetGood = GoodGUID.LoadGood(AssetDatabaseUtility.GetResourceLoadPath(assetPath.stringValue));
			if (targetGood != null) goodID.stringValue = targetGood.ID;
		}
	}
}