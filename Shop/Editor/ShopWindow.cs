﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace YOZH.Plugins.Shop.Editor
{
#pragma warning disable 0219, 0414
	/// <summary>
	/// Not completed yet.
	/// It should draw beautiful window to allow game designer setup shop.
	/// Now it can be done through default scriptable object inspector
	/// </summary>
	public class ShopWindow : EditorWindow
	{
		[MenuItem("HBG/Shop/ShopWindow")]
		public static void Open()
		{
			EditorWindow.GetWindow<ShopWindow>(true, "Shop Editor", true);
		}

		private UnityEditor.Editor shopEditor;
		private UnityEditor.Editor storeEditor;

		private Vector2 leftScrollPos = Vector2.zero;
		private Vector2 middleScrollPos = Vector2.zero;
		private Vector2 rightScrollPos = Vector2.zero;
		private TreeviewItem treeView = null;


		private void OnEnable()
		{
			shopEditor = UnityEditor.Editor.CreateEditor(Shop.Instance);
			storeEditor = UnityEditor.Editor.CreateEditor(ShopStore.Instance);
			treeView = new TreeviewItem(shopEditor.serializedObject.FindProperty("rootCategory"));
		}

		private void OnGUI()
		{

			EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

			DrawLeftPanel();
			DrawMiddlePanel();
			DrawRightPanel();

			EditorGUILayout.EndHorizontal();

			shopEditor.serializedObject.ApplyModifiedProperties();
			storeEditor.serializedObject.ApplyModifiedProperties();
		}

		private static void DebugRect(Rect rect)
		{
			EditorGUI.LabelField(rect, "", GUI.skin.box);
		}
		private static void DebugRect()
		{
			EditorGUILayout.LabelField("", GUI.skin.box, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
		}

		private void DrawLeftPanel()
		{
			EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(position.width / 3), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

			leftScrollPos = EditorGUILayout.BeginScrollView(leftScrollPos, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true));
			treeView.Draw();
			EditorGUILayout.EndScrollView();

			EditorGUILayout.EndVertical();
		}
		private void DrawMiddlePanel()
		{
			EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(position.width / 3), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
			EditorGUILayout.EndVertical();
		}
		private void DrawRightPanel()
		{
			EditorGUILayout.BeginVertical(GUI.skin.box, GUILayout.Width(position.width / 3), GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

			rightScrollPos = EditorGUILayout.BeginScrollView(leftScrollPos, GUILayout.ExpandWidth(false), GUILayout.ExpandHeight(true));
			EditorGUILayout.PropertyField(storeEditor.serializedObject.FindProperty("items"), true);
			EditorGUILayout.EndScrollView();

			EditorGUILayout.EndVertical();
		}

		private class TreeviewItem
		{
			public bool IsSelected { get; private set; }
			public SerializedProperty Target { get; private set; }
			public string ID { get { return Target.FindPropertyRelative("id").stringValue; } }
			public TreeviewItem Parent { get; set; }
			public List<TreeviewItem> Children = new List<TreeviewItem>();

			public TreeviewItem(SerializedProperty target)
			{
				Target = target;

				SerializedProperty childrenProp = Target.FindPropertyRelative("subCategories");
				if (childrenProp != null && childrenProp.isArray) {
					for (int index = 0; index < childrenProp.arraySize; index++) {
						addChild(childrenProp.GetArrayElementAtIndex(index));
					}
				}
				SerializedProperty productsProp = Target.FindPropertyRelative("products");
				if (productsProp != null && productsProp.isArray) {
					for (int index = 0; index < productsProp.arraySize; index++) {
						addChild(productsProp.GetArrayElementAtIndex(index));
					}
				}

				Target.isExpanded = false;
			}

			public void Draw()
			{
				updateChildren();
				EditorGUILayout.BeginVertical();
				drawHeader();

				if (Target.isExpanded) {
					drawChildren();
				}
				EditorGUILayout.EndVertical();
			}

			private void updateChildren()
			{
				SerializedProperty childrenProp = Target.FindPropertyRelative("subCategories");
				if (childrenProp != null && childrenProp.isArray) {
					for (int index = 0; index < childrenProp.arraySize; index++) {
						SerializedProperty childProp = childrenProp.GetArrayElementAtIndex(index);
					}
				}
			}

			private void addChild(SerializedProperty prop)
			{
				if (Children.Find(item => item.Target == prop) != null) return;

				TreeviewItem targetChild = new TreeviewItem(prop);

				targetChild.Parent = this;
				Children.Add(targetChild);
			}

			private void removeChild(SerializedProperty prop)
			{
				TreeviewItem targetChild = Children.Find(item => item.Target == prop);
				if (targetChild == null) return;

				targetChild.Parent = null;
				Children.Remove(targetChild);
			}

			private void drawHeader()
			{
				EditorGUILayout.BeginHorizontal();

				if (Children.Count > 0) {
					GUILayout.Space(EditorGUIUtility.singleLineHeight * EditorGUI.indentLevel);
					Target.isExpanded = EditorGUILayout.Toggle(Target.isExpanded,
															   EditorStyles.foldout,
															   GUILayout.Width(EditorGUIUtility.singleLineHeight),
															   GUILayout.Height(EditorGUIUtility.singleLineHeight));
				}
				else {
					GUILayout.Space(EditorGUIUtility.singleLineHeight * (EditorGUI.indentLevel + 1) + EditorGUIUtility.standardVerticalSpacing * 2);
				}

				GUIContent label = new GUIContent("null");
				if (Target != null) {
					label = new GUIContent(Target.name);
					if (Target.type == typeof(Category).Name) {
						label.image = Target.isExpanded ? EditorGUIUtility.FindTexture("FolderEmpty Icon") : EditorGUIUtility.FindTexture("Folder Icon");
					}
					if (Target.type == typeof(IProduct).Name) {
						label.image = EditorGUIUtility.FindTexture("ScriptableObject Icon");
					}
				}

				GUIStyle labelStyle = EditorStyles.largeLabel;

				if (GUILayout.Button(label, labelStyle, GUILayout.ExpandWidth(true), GUILayout.MaxHeight(EditorGUIUtility.singleLineHeight * 1.5f))) {
					Debug.Log(Target.type);
				}

				EditorGUILayout.EndHorizontal();
			}

			private void drawChildren()
			{
				EditorGUI.indentLevel++;
				foreach (TreeviewItem item in Children) {
					item.Draw();
				}
				EditorGUI.indentLevel--;
			}
		}
	}
#pragma warning restore 0219, 0414
}