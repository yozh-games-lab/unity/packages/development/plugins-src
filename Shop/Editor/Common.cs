﻿using UnityEditor;

namespace YOZH.Plugins.Shop.Editor
{
	public class Common : UnityEditor.Editor
	{
		public const string ASSETS_PATH = "HBG/Resources";

		[MenuItem("HBG/Shop/Create shop assets")]
		public static void CreateAssets()
		{
			if (!Shop.Instance) ShopEditor.CreateAsset();
			if (!ShopStore.Instance) ShopStoreEditor.CreateAsset();
		}
	}
}