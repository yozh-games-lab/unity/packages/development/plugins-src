﻿using UnityEditor;
using YOZH.Core.Misc.Editor;
using System.IO;

namespace YOZH.Plugins.Shop.Editor
{
	[CustomEditor(typeof(ShopStore))]
	public class ShopStoreEditor : UnityEditor.Editor
	{
		public static void CreateAsset()
		{
			ScriptableObjectUtility.CreateScriptableObjectAtPath<ShopStore>(Path.Combine(Common.ASSETS_PATH, ShopStore.RESOURCE_PATH));
		}
	}
}