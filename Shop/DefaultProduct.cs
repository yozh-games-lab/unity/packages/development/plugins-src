﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Shop
{
	public class DefaultProduct : ShopItem, IProduct
	{
		[SerializeField, GoodID]
		private string goodID = null;
		public string GoodID { get { return goodID; } }

		[SerializeField]
		private List<ExchangeRate> rates = new List<ExchangeRate>();
		public ExchangeRate[] Rates { get { return rates.ToArray(); } }

		public bool GetRate(string curencyId, out ExchangeRate rate)
		{
			rate = rates.Find(item => item.CurrencyId == curencyId);
			return rate != null;
		}
	}
}