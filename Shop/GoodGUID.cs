﻿using UnityEngine;
using System;
using YOZH.Core.GUIDLinks;

namespace YOZH.Plugins.Shop
{

	[Serializable]
	public class GoodGUID : GUIDLink<IGood>
	{
		[SerializeField]
		private string id = "";
		public string ID {
			get {
				if (string.IsNullOrEmpty(id)) {
					return getInstance().ID;
				}
				return id;
			}
		}

		public static IGood LoadGood(string path)
		{
			UnityEngine.Object obj = Resources.Load<UnityEngine.Object>(path);
			return (obj is GameObject) ? ((GameObject)obj).GetComponent<IGood>() : obj as IGood;
		}

		protected override IGood getInstance()
		{
			return LoadGood(loadPath);
		}
	}
}