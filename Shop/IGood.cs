﻿using System;
using YOZH.Core.Misc.Interfaces;
using YOZH.Plugins.UIExt;

namespace YOZH.Plugins.Shop
{
	public interface IGood : IUIPresentable, ICloneableCountable, IDisposable
	{
		string ID { get; }
		void Validate(GoodValidationCallbak callback);
	}

	public delegate void GoodValidationCallbak(bool isAccepted);
}