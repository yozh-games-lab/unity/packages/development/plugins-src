﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.Localization
{
	[RequireComponent(typeof(Text))]
	public class LocalizedUiTextFont : LocalizedObject
	{
		private readonly LocalizedFont lFont = new LocalizedFont();
		private Text text = null;

		public override void Init()
		{
			text = GetComponent<Text>();
		}

		public override void UpdateContent()
		{
			text.font = (Font)lFont;
		}
	}
}