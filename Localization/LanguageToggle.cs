﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.Localization
{
	public class LanguageToggle : MonoBehaviour
	{
		[SerializeField]
		private SystemLanguage language = SystemLanguage.Unknown;
		[SerializeField]
		private Toggle toggle = null;

		void Awake()
		{
			if (toggle == null)
				toggle = GetComponent<Toggle>();

			toggle.isOn = (Localization.Instance.CurrentLanguage.name == language.ToString());
		}

		public void OnValueChanged()
		{
			if (toggle.isOn) {
				Localization.Instance.ChooseLanguage(language);

			}
		}
	}
}