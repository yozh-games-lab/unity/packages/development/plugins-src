﻿using System.IO;
using YOZH.Core.Misc.Editor;
using UnityEditor;
using UnityEngine;

namespace YOZH.Plugins.Localization.Editor
{
	[CustomEditor(typeof(Localization))]
	public class LocalizationEditor : UnityEditor.Editor
	{
		private SystemLanguage tmpLang;

		public override void OnInspectorGUI()
		{
			GUI.skin.label.alignment = TextAnchor.MiddleLeft;

			GUI.enabled = false;
			SerializedProperty loadStyle = serializedObject.FindProperty("loadStyle");
			EditorGUILayout.PropertyField(loadStyle);
			EditorGUILayout.Space();
			GUI.enabled = true;

			SerializedProperty defaultLang = serializedObject.FindProperty("defaultLanguage");
			EditorGUILayout.PropertyField(defaultLang);
			SerializedProperty defaultFont = serializedObject.FindProperty("defaultFont");
			EditorGUILayout.PropertyField(defaultFont);

			SerializedProperty languageList = serializedObject.FindProperty("languageList");

			if (languageList.arraySize < 1) {
				if (!langListContains(languageList, Application.systemLanguage)) {
					CreateNewLanguageAsset(Application.systemLanguage);
					languageList.InsertArrayElementAtIndex(languageList.arraySize);
					languageList.GetArrayElementAtIndex(languageList.arraySize - 1).intValue = (int)Application.systemLanguage;
				}
			}

			if (!langListContains(languageList, (SystemLanguage)defaultLang.intValue)) {
				CreateNewLanguageAsset((SystemLanguage)defaultLang.intValue);
				languageList.InsertArrayElementAtIndex(languageList.arraySize);
				languageList.GetArrayElementAtIndex(languageList.arraySize - 1).intValue = defaultLang.intValue;
			}

			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Available languages:");
			for (int i = 0; i < languageList.arraySize; i++) {
				SystemLanguage lang = (SystemLanguage)languageList.GetArrayElementAtIndex(i).intValue;
				EditorGUILayout.BeginHorizontal();

				if ((GUILayout.Button(lang.ToString(), GUI.skin.box, GUILayout.ExpandWidth(true)))) {
					string path = Core.Misc.MiscUtilities.NormalizePath(string.Format("Assets/{0}/{1}/{2}/{3}.asset", Localization.ASSETS_RELATIVE_PATH, Localization.PATH_TO_LOCALIZATION, Localization.PATH_TO_LANGUAGES, lang));
					Debug.Log(path);

					ScriptableObject targetAtPath = AssetDatabase.LoadAssetAtPath<Language>(path);
					if (targetAtPath != null) {
						Selection.activeObject = targetAtPath;
					}
				}
				GUI.enabled = (lang != (SystemLanguage)defaultLang.intValue);

				if (GUILayout.Button("Remove", GUILayout.ExpandWidth(false))) {
					DeleteLanguageAsset(lang);
					languageList.DeleteArrayElementAtIndex(i);
				}
				GUI.enabled = true;
				EditorGUILayout.EndHorizontal();
			}

			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
			tmpLang = (SystemLanguage)EditorGUILayout.EnumPopup(tmpLang);

			GUI.enabled = !langListContains(languageList, tmpLang);
			if (GUILayout.Button(string.Format("Add {0}", tmpLang))) {
				CreateNewLanguageAsset(tmpLang);
				languageList.InsertArrayElementAtIndex(languageList.arraySize);
				languageList.GetArrayElementAtIndex(languageList.arraySize - 1).intValue = (int)tmpLang;
			}
			GUI.enabled = true;
			EditorGUILayout.EndHorizontal();

			serializedObject.ApplyModifiedProperties();
		}

		private bool langListContains(SerializedProperty langList, SystemLanguage lang)
		{
			if (!langList.isArray) return false;

			for (int i = 0; i < langList.arraySize; i++)
				if ((SystemLanguage)langList.GetArrayElementAtIndex(i).intValue == lang)
					return true;
			return false;
		}

		public static void CreateNewLanguageAsset(SystemLanguage lang)
		{
			string path = Core.Misc.MiscUtilities.NormalizePath(string.Format("{0}/{1}/{2}", Localization.ASSETS_RELATIVE_PATH, Localization.PATH_TO_LOCALIZATION, Localization.PATH_TO_LANGUAGES));

			ScriptableObjectUtility.CreateScriptableObjectAtPath<Language>(string.Format("Assets/{0}/{1}", path, lang));
			string folderPath = string.Format("Assets/{0}", path);
			string folderName = string.Format("{0}_files", lang);
			if (!AssetDatabase.IsValidFolder(string.Format("{0}/{1}", folderPath, folderName))) {
				AssetDatabase.CreateFolder(folderPath, folderName);
			}

			string emptyFilePath = string.Format("{0}/{1}/{2}_files/.empty", Application.dataPath, path, lang);
			if (!File.Exists(emptyFilePath)) {
				File.Create(emptyFilePath);
			}
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}

		public static void DeleteLanguageAsset(SystemLanguage lang)
		{
			string path = Core.Misc.MiscUtilities.NormalizePath(string.Format("{0}/{1}/{2}", Localization.ASSETS_RELATIVE_PATH, Localization.PATH_TO_LOCALIZATION, Localization.PATH_TO_LANGUAGES));

			AssetDatabase.DeleteAsset(string.Format("Assets/{0}/{1}.asset", path, lang));

			string dirPath = string.Format("{0}/{1}_files", path, lang);
			string fullPath = string.Format("{0}/{1}", Application.dataPath, dirPath);

			if (Directory.Exists(fullPath)) {
				string[] files = Directory.GetFiles(fullPath);
				bool hasFiles = false;
				foreach (string file in files) {
					if (Path.GetFileName(file) != ".empty") {
						hasFiles = true;
						break;
					}
				}

				if (!hasFiles) {
					string assetDirPath = string.Format("Assets/{0}", dirPath);
					AssetDatabase.DeleteAsset(assetDirPath);
				}
			}
			AssetDatabase.SaveAssets();
			AssetDatabase.Refresh();
		}
	}
}