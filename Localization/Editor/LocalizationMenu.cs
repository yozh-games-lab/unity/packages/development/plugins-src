﻿using YOZH.Core.Misc.Editor;
using UnityEditor;

namespace YOZH.Plugins.Localization.Editor
{
	public class LocalizationMenu : UnityEditor.Editor
	{
		[MenuItem("HBG/Localization/Create localization")]
		[MenuItem("Assets/Create/New localization")]
		public static void NewLocalization()
		{
			ScriptableObjectUtility.CreateScriptableObjectAtPath<Localization>(string.Format("Assets/{0}/{1}/Localization", Localization.ASSETS_RELATIVE_PATH, Localization.PATH_TO_LOCALIZATION), true);
		}
	}
}