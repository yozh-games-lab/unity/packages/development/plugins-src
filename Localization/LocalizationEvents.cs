﻿using UnityEngine;
using UnityEngine.SceneManagement;


namespace YOZH.Plugins.Localization
{
	public class LocalizationEvents : MonoBehaviour
	{
		private static LocalizationEvents instance;
		public static LocalizationEvents Instance {
			get {
				if (instance == null) {
					instance = Instantiate();
				}
				return instance;
			}
		}

		private void Awake()
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
			SceneManager.sceneLoaded += onSceneLoaded;
		}
		private void OnDestroy()
		{
			SceneManager.sceneLoaded -= onSceneLoaded;
		}
		public static LocalizationEvents Instantiate()
		{
			return new GameObject("LocalizationEvents").AddComponent<LocalizationEvents>();
		}
		private void onSceneLoaded(Scene arg0, LoadSceneMode arg1)
		{
			if (Localization.Instance.LoadStyle == LocalizationLoadStyle.ByScene) {
				Localization.Instance.CurrentLanguage.LoadSection(SceneManager.GetActiveScene().name);
			}
		}
	}
}