﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Localization
{
	[System.Serializable]
	public class TextSection : LocaleSection
	{
		public int KeyColl = 0;
		public int ValColl = 1;

		#region Constructors
		public TextSection(LocaleSectionNames name, int keyColl, int valColl)
		{
			Name = name;
			KeyColl = keyColl;
			ValColl = valColl;
		}
		public TextSection(LocaleSectionNames name) : this(name, 0, 1) { }
		#endregion

		#region Implemented abstract members of LocaleSection
		public override void Load(Dictionary<string, object> dict)
		{
			string[] paths = {
				Core.Misc.MiscUtilities.NormalizePath(string.Format(@"{0}/{1}/{2}", Localization.PATH_TO_LOCALIZATION, Localization.PATH_TO_LANGUAGES, Name)),
				Core.Misc.MiscUtilities.NormalizePath(string.Format(@"{0}/{1}/{2}_files/{3}", Localization.PATH_TO_LOCALIZATION, Localization.PATH_TO_LANGUAGES, Localization.Instance.CurrentLanguage.name, Name))
			};

			TextAsset csv = null;
			int i = 0;
			do {
				csv = Resources.Load<TextAsset>(paths[i]);
				i++;
			} while (csv == null && i < paths.Length);

			if (csv != null) {
				string[] lines = csv.text.Split(new[] { "\n" }, System.StringSplitOptions.None);
				foreach (string line in lines) {
					string[] colls = line.Split(new[] { "\t" }, System.StringSplitOptions.None);
					if (colls.Length > Mathf.Max(KeyColl, ValColl) && !dict.ContainsKey(colls[KeyColl])) {
						dict.Add(colls[KeyColl], colls[ValColl]);
					}
				}
			}
			Resources.UnloadAsset(csv);
		}

		#endregion
	}
}