﻿using UnityEngine;

namespace YOZH.Plugins.Localization
{
	public class LocalizedFont : LocalizedType
	{
		public Font Value {
			get {
				if (Localization.Instance.CurrentLanguage != language) {
					language = Localization.Instance.CurrentLanguage;
				}
				return language.Font;
			}
		}

		public static explicit operator Font(LocalizedFont lFont)
		{
			return lFont.Value;
		}
	}
}