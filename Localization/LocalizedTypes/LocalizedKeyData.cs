﻿using UnityEngine;

namespace YOZH.Plugins.Localization
{
	public class LocalizedKeyData : LocalizedType
	{
		[SerializeField]
		protected string key;
		public string Key { get { return key; } }

		public LocalizedKeyData(string key)
		{
			this.key = key;
		}
	}
}