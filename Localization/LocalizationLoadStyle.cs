﻿namespace YOZH.Plugins.Localization
{
	public enum LocalizationLoadStyle
	{
		AllAtOnce = 0,
		ByScene
	}
}