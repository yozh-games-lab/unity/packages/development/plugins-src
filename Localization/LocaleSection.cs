﻿using System.Collections.Generic;

namespace YOZH.Plugins.Localization
{
	[System.Serializable]
	public abstract class LocaleSection
	{
		public LocaleSectionNames Name = LocaleSectionNames.Common;
		public abstract void Load(Dictionary<string, object> dict);
	}
}