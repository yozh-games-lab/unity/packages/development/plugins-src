﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.Localization
{
	public class Localization : ScriptableObject
	{
		#region Constants
		public const string ASSETS_RELATIVE_PATH = "HBG/Resources"; //EDITORS ONLY
		public const string PATH_TO_LOCALIZATION = "Localization";
		public const string PATH_TO_LANGUAGES = "Langs";
		public const string ACTIVE_LANG_PREF_KEY = "CurrentLang";
		#endregion

		#region Singleton
		private static Localization instance;
		public static Localization Instance {
			get {
				if (instance == null) {
					instance = Resources.Load<Localization>(Core.Misc.MiscUtilities.NormalizePath(string.Format("{0}/{1}", PATH_TO_LOCALIZATION, typeof(Localization))));

					string activeLang = PlayerPrefs.GetString(ACTIVE_LANG_PREF_KEY, Application.systemLanguage.ToString());
					if (instance.languageList.Contains((SystemLanguage)Enum.Parse(typeof(SystemLanguage), activeLang))) {
						instance.ChooseLanguage(activeLang);
					}
					else {
						instance.ChooseLanguage(instance.defaultLanguage);
					}
					if (instance.loadStyle == LocalizationLoadStyle.ByScene) {
						LocalizationEvents.Instantiate();
					}
				}
				return instance;
			}
		}
		#endregion

		#region Properties
		[SerializeField]
		private Language currentLanguage = null;
		public Language CurrentLanguage {
			get { return currentLanguage; }
		}

		[SerializeField]
		private SystemLanguage defaultLanguage = SystemLanguage.English;
		public SystemLanguage DefaultLanguage {
			get { return defaultLanguage; }
		}

		[SerializeField]
		private Font defaultFont = null;
		public Font DefaultFont {
			get {
				if (defaultFont == null) defaultFont = Resources.GetBuiltinResource<Font>("Arial.ttf");
				return defaultFont;
			}
		}

		[SerializeField]
		private List<SystemLanguage> languageList = new List<SystemLanguage>();

		[SerializeField]
		private LocalizationLoadStyle loadStyle = LocalizationLoadStyle.AllAtOnce;
		public LocalizationLoadStyle LoadStyle {
			get { return loadStyle; }
		}
		#endregion

		#region Events
		public delegate void OnLanguageChanged();
		public event OnLanguageChanged onLanguageChanged;
		#endregion

		#region Methods
		public void ChooseLanguage(SystemLanguage lang)
		{
			ChooseLanguage(lang.ToString());
		}
		public void ChooseLanguage(string langName)
		{
			if (currentLanguage != null)
				currentLanguage.Unload();

			string path = string.Format(@"{0}/{1}/{2}", PATH_TO_LOCALIZATION, PATH_TO_LANGUAGES, langName);
			path = Core.Misc.MiscUtilities.NormalizePath(path);
			currentLanguage = Resources.Load<Language>(path);

			if (currentLanguage == null) {
				path = string.Format(@"{0}/{1}/{2}", PATH_TO_LOCALIZATION, PATH_TO_LANGUAGES, defaultLanguage);
				path = Core.Misc.MiscUtilities.NormalizePath(path);
				currentLanguage = Resources.Load<Language>(path);
			}

			switch (loadStyle) {
				case LocalizationLoadStyle.ByScene:
					currentLanguage.Load(false);
					break;
				case LocalizationLoadStyle.AllAtOnce:
					break;
				default:
					currentLanguage.Load(true);
					break;
			}
			PlayerPrefs.SetString(ACTIVE_LANG_PREF_KEY, currentLanguage.name);

			if (onLanguageChanged != null) onLanguageChanged();
		}
		#endregion
	}
}