﻿namespace YOZH.Plugins.UIExt
{
	public interface IUIPresentable
	{
		UIPresentation UIPresentation { get; }
	}
}