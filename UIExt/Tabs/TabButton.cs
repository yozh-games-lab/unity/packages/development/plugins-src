﻿using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	public class TabButton : MonoBehaviour
	{
		public bool Selected = false;
		public TabGroup Group = null;
		public TabBody Body = null;

		void Awake()
		{
			if (Group != null)
				Group.AddTab(this);
			if (Group != null && Selected)
				Group.SelectTab(this);

			if (Selected)
				SelectOn();
			else
				SelectOff();
		}

		public void SelectOn()
		{
			if (Body != null)
				Body.SwitchOn();
			Selected = true;
		}

		public void SelectOff()
		{
			if (Body != null)
				Body.SwitchOff();
			Selected = false;
		}

		public void Switch()
		{
			if (Group != null) {
				if (!Selected)
					Group.SelectTab(this);
			}
			else {
				if (Selected)
					Body.SwitchOff();
				else
					Body.SwitchOn();
			}
		}
	}
}