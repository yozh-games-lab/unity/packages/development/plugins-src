﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	public class TabGroup : MonoBehaviour
	{
		private List<TabButton> tabs = new List<TabButton>();

		public void SelectTab(TabButton button)
		{
			foreach (TabButton tab in tabs) {
				if (tab != button) {
					tab.SelectOff();
				}
			}
			button.SelectOn();
		}

		public void AddTab(TabButton tabButton)
		{
			if (!tabs.Contains(tabButton)) {
				tabs.Add(tabButton);
			}
		}

		public void RemoveTab(TabButton tabButton)
		{
			tabs.Remove(tabButton);
		}
	}
}