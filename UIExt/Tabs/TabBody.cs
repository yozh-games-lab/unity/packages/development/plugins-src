﻿using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	public class TabBody : MonoBehaviour
	{
		public void SwitchOn()
		{
			gameObject.SetActive(true);
			transform.SetAsLastSibling();
		}

		public void SwitchOff()
		{
			gameObject.SetActive(false);
		}
	}
}