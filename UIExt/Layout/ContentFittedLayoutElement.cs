﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.UIExt.Layout
{
	[ExecuteInEditMode]
	public class ContentFittedLayoutElement : LayoutElement
	{
		[SerializeField]
		private RectOffset borders = new RectOffset();

		[SerializeField]
		private RectTransform referencedChild = null;

		[SerializeField]
		private bool useReferencedWidth = true;
		[SerializeField]
		private bool useReferencedHeight = true;

		public override void CalculateLayoutInputHorizontal()
		{
			if (referencedChild != null && useReferencedWidth) {
				preferredWidth = referencedChild.rect.width + borders.left + borders.right;
			}
			base.CalculateLayoutInputHorizontal();
		}

		public override void CalculateLayoutInputVertical()
		{
			if (referencedChild != null && useReferencedHeight) {
				preferredHeight = referencedChild.rect.height + borders.top + borders.bottom;
			}
			base.CalculateLayoutInputVertical();
		}

#if UNITY_EDITOR
		private void Update()
		{
			CalculateLayoutInputHorizontal();
			CalculateLayoutInputVertical();
		}
#endif

	}
}