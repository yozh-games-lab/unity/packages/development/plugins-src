﻿using UnityEditor;

namespace YOZH.Plugins.UIExt.Layout.Editor
{
	[CustomEditor(typeof(ContentFittedLayoutElement))]
	public class ContentFittedLayoutElementEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
		}
	}
}