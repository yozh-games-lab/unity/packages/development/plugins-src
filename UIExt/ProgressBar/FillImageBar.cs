﻿using UnityEngine;
using UnityEngine.UI;


namespace YOZH.Plugins.UIExt
{
	[RequireComponent(typeof(Image))]
	public class FillImageBar : MonoBehaviour, IProgressBar
	{
		private Image thisImage = null;

		#region IProgressBar implementation
		public float Value {
			get {
				return thisImage.fillAmount;
			}
			set {
				thisImage.fillAmount = Mathf.Clamp01(value);
			}
		}
		#endregion

		#region IGameObjectComponent implementation

		public T FindComponent<T>() where T : Component
		{
			return GetComponent<T>();
		}

		public GameObject GameObject { get { return gameObject; } }

		#endregion

		private void Awake()
		{
			thisImage = GetComponent<Image>();
		}
	}
}