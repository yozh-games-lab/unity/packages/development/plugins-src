﻿using YOZH.Core.Misc.Interfaces;

namespace YOZH.Plugins.UIExt
{
	public interface IProgressBar : IGameObjectComponent
	{
		float Value { get; set; }
	}
}