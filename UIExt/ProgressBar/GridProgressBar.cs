﻿using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	public class GridProgressBar : MonoBehaviour, IGridProgressBar
	{
		[SerializeField]
		private SerializableIGridBarCell cellSample = new SerializableIGridBarCell();
		public IGridBarCell CellSample {
			get {
				return cellSample;
			}
			set {
				if (cellSample.Instance != value.Instance) {
					foreach (IGridBarCell cell in cellList) {
						Destroy(cell.Instance.gameObject);
					}
					cellList.Clear();

					IGridBarCell clone = value.Clone() as IGridBarCell;

					clone.Instance.transform.SetParent(cellSample.Instance.transform.parent, false);
					Destroy(cellSample.Instance.gameObject);
					cellSample.CellGameObject = clone.Instance.gameObject;

					updateGrid();
				}
			}
		}

		private List<IGridBarCell> cellList = new List<IGridBarCell>();

		private float delta = 0;
		public float Delta {
			get {
				return delta;
			}

			set {
				if (delta != value) {
					delta = normalizeDeltaValue(value);
					updateProgress();
				}

			}
		}

		[SerializeField]
		private int cellCount = 10;
		public int CellCount {
			get {
				return cellCount;
			}

			set {
				if (cellCount != value) {
					cellCount = value;
					updateGrid();
					updateProgress();
				}
			}
		}

		private float value = 0;
		public float Value {
			get {
				return value;
			}

			set {
				if (this.value != value) {
					this.value = normalizeValue(value);
					updateProgress();
				}
			}
		}

		public GameObject GameObject {
			get {
				return gameObject;
			}
		}

		private void Awake()
		{
			updateGrid();
			updateProgress();
		}

		private void updateGrid()
		{
			if (cellCount < 0)
				return;

			cellSample.SetActive(false);

			if (cellCount > cellList.Count) {
				for (int i = cellList.Count; i < cellCount; i++) {
					cellList.Add((IGridBarCell)cellSample.Clone());
					cellList[i].SetActive(true);
				}
			}
			else if (cellCount <= cellList.Count) {
				while (cellList.Count > cellCount) {
					IGridBarCell currentGrid = cellList[cellList.Count - 1];
					Destroy(currentGrid.Instance.gameObject);
					cellList.Remove(currentGrid);
				}
			}
		}

		private void updateProgress()
		{
			if (cellCount < 0)
				return;

			for (int i = 1; i <= cellList.Count; i++) {
				IGridBarCell cell = cellList[i - 1];

				float percent = ((float)(i) / cellCount);
				if (percent <= Value) {
					cell.State = GridBarCellState.Enabled;
				}
				else if (percent <= (Value + Delta)) {
					cell.State = GridBarCellState.ProgressDelta;
				}
				else {
					cell.State = GridBarCellState.Disabled;
				}
			}
		}

		protected float normalizeValue(float val)
		{
			if (val > 1)
				val = 1;
			else if (val < 0)
				val = 0;
			return val;
		}

		private float normalizeDeltaValue(float delta)
		{
			if (delta > 1 - Value) delta = 1 - Value;
			delta = normalizeValue(delta);
			return delta;
		}

		public T FindComponent<T>() where T : Component
		{
			return GetComponent<T>();
		}
	}
}