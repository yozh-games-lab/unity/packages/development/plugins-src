﻿using System;
using UnityEngine;

namespace YOZH.Plugins.UIExt
{
	[Serializable]
	public class UIPresentation
	{
		[SerializeField]
		private string name = "";
		public string Name { get { return name; } }

		[SerializeField]
		private string description = "";
		public string Description { get { return description; } }

		[SerializeField]
		private Sprite icon = null;
		public Sprite Icon { get { return icon; } }
	}
}