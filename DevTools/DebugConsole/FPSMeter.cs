﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace YOZH.Plugins.DevTools.DebugConsole
{
	[RequireComponent(typeof(Text))]
	public class FPSMeter : MonoBehaviour
	{
		[SerializeField]
		private float frequency = 1.0f;
		private Text thisText;

		private void Awake()
		{
			thisText = GetComponent<Text>();
		}

		private void OnEnable()
		{
			StartCoroutine(updateRoutine());
		}

		private IEnumerator updateRoutine()
		{
			while (true) {
				int lastFrameCount = Time.frameCount;
				float lastTime = Time.realtimeSinceStartup;

				yield return new WaitForSeconds(frequency);

				float timeSpan = Time.realtimeSinceStartup - lastTime;
				int frameCount = Time.frameCount - lastFrameCount;

				thisText.text = string.Format("FPS: {0}", Mathf.RoundToInt(frameCount / timeSpan));
			}
		}
	}
}