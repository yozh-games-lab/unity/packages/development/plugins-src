﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace YOZH.Plugins.DevTools.DebugConsole
{
	public class ConsoleOpenButton : MonoBehaviour, IDragHandler, IPointerClickHandler
	{

		[SerializeField]
		GameObject target = null;

		public void OnDrag(PointerEventData eventData)
		{
			transform.position = eventData.position;
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			if (!eventData.dragging) {
				gameObject.SetActive(false);
				target.SetActive(true);
			}
		}
	}
}