﻿using UnityEditor;

namespace YOZH.Plugins.DevTools.Editor
{
	public class FbxFixImportMaterials : AssetPostprocessor
	{
		private void OnPreprocessModel()
		{
			ModelImporter modelImporter = assetImporter as ModelImporter;
			modelImporter.importMaterials = false;
		}
	}
}