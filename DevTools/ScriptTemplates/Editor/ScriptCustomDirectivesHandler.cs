using UnityEngine;
using UnityEditor;
using System.IO;

namespace YOZH.Plugins.DevTools.ScriptTemplates.Editor
{
	public class ScriptCustomDirectivesHandler : AssetPostprocessor
	{
		private const string NAMESPACE_PATH = "NAMESPACE_PATH";
		private const string DEFAULT_NAMESPACE = "HBG";

		public int callbackOrder { get { return 0; } }

		static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
		{
			foreach (string assetPath in importedAssets) {
				if (isTargetScript(assetPath)) {
					TextAsset scriptAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(assetPath);
					string sourceCode = scriptAsset.text.Replace(string.Format("#{0}#", NAMESPACE_PATH), getNameSpace(assetPath));
					if (sourceCode != scriptAsset.text) {
						File.WriteAllText(assetPath, sourceCode);
					}
				}
			}
		}

		private static bool isTargetScript(string assetPath)
		{
			return (Path.GetExtension(assetPath) == ".cs" || Path.GetExtension(assetPath) == ".js");
		}

		private static string getNameSpace(string assetPath)
		{
			string result = Path.GetDirectoryName(assetPath);
			result = result.TrimStart($"Assets{Path.DirectorySeparatorChar}".ToCharArray());
			result = result.Replace(Path.DirectorySeparatorChar.ToString(), ".");

			if (string.IsNullOrEmpty(result)) result = EditorSettings.projectGenerationRootNamespace;
			if (string.IsNullOrEmpty(result)) result = DEFAULT_NAMESPACE;
			return result;
		}
	}
}