﻿using UnityEngine;
using System.Collections.Generic;

namespace YOZH.Plugins.DevTools
{
	public class LegacyGUIConsole : MonoBehaviour
	{
		public Texture2D Background;

		private bool isShow = false;
		private List<LogMessage> content = new List<LogMessage>();
		private Vector2 scrollPosition = Vector2.zero;

		private static LegacyGUIConsole instance;
		public static LegacyGUIConsole Instance {
			set {
				if (instance == null) instance = value;
				else Destroy(value);
			}
			get {
				if (instance == null) {
					GameObject obj = new GameObject();
					obj.name = "HBGConsole";
					instance = obj.AddComponent<LegacyGUIConsole>();
				}
				return instance;
			}
		}

		void Awake()
		{
			Instance = this;
			DontDestroyOnLoad(gameObject);

			if (Background == null) {
				Background = new Texture2D(1, 1);
				for (int i = 0; i < Background.width; i++) {
					for (int j = 0; j < Background.height; j++) {
						Background.SetPixel(i, j, new Color32(0, 0, 0, 125));
					}
				}
				Background.Apply();
			}
		}

		void Start()
		{
			Application.logMessageReceived += OnLogAdded;
		}

		private void OnLogAdded(string condition, string stackTrace, LogType type)
		{
			LogMessage message = new LogMessage
			{
				Text = condition,
				Type = type,
				Stack = stackTrace
			};
			content.Add(message);
		}

		void OnGUI()
		{
#if !UNITY_EDITOR
            if (GUI.Button(new Rect((Screen.width - Screen.width * 0.1f) - 5, 5, Screen.width * 0.1f, Screen.height * 0.1f), "+")) {
                isShow = !isShow;
            }
#endif
			if (!isShow) return;
			GUIStyle style = new GUIStyle(GUI.skin.window);
			style.normal.background = Background;

			GUILayout.BeginArea(new Rect(5, (Screen.height / 3.0f), Screen.width - 10, ((Screen.height / 3) * 2) - 5), "DEV console", style);
			scrollPosition = GUILayout.BeginScrollView(scrollPosition);

			foreach (LogMessage message in content) {
				switch (message.Type) {
					case LogType.Log:
						GUI.color = Color.green;
						break;

					case LogType.Warning:
						GUI.color = Color.yellow;
						break;

					case LogType.Error:
					case LogType.Exception:
						GUI.color = Color.red;
						break;
				}
				string messageText = message.Text;
				if (message.Type == LogType.Exception) {
					messageText += "\n" + message.Stack;
				}
				GUILayout.Label(messageText);
			}
			GUILayout.EndScrollView();
			GUILayout.EndArea();
		}

		struct LogMessage
		{
			public string Text;
			public string Stack;
			public LogType Type;
		}
	}
}