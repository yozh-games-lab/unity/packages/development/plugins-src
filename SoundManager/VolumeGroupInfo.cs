﻿using UnityEngine;

namespace YOZH.Plugins.SoundManager
{
	[System.Serializable]
	public class VolumeGroupInfo
	{
		[SerializeField]
		private string name;
		public string Name {
			get {
				return name;
			}
		}

		[SerializeField, Range(0, 1)]
		private float volume = 0.5f;
		public float Volume {
			get { return volume; }
			set {
				if (value >= 0 && value <= 1)
					volume = value;
				else
					if (value < 0)
					volume = 0;
				else
					if (value > 1)
					volume = 1;
			}
		}

		public VolumeGroupInfo(string name, float initVolume)
		{
			this.name = name;
			Volume = initVolume;
		}

		public VolumeGroupInfo()
		{
			name = typeof(VolumeGroup).ToString();
			Volume = 0.5f;
		}

		public static explicit operator VolumeGroup(VolumeGroupInfo volumeInfo)
		{
			return new VolumeGroup(volumeInfo);
		}
	}
}