﻿using UnityEngine;
using System.Collections;
using YOZH.Core.CustomAttributes;


namespace YOZH.Plugins.SoundManager
{
	public class SoundNameAttribute : PopupAttribute
	{
		public SoundNameAttribute()
		{
			Choices = AudioLibrary.Instance.GetSoundsNames();
		}
	}
}