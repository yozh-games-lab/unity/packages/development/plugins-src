using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using YOZH.Core.Misc;


namespace YOZH.Plugins.SoundManager
{
	public class AudioPlayer : MonoBehaviour
	{
		[SerializeField]
		protected Sound currentSound = null;

		[SerializeField, Space]
		protected bool allowLoopedSounds = false;

		[SerializeField, Space]
		protected float firstMinDelay = 0;
		[SerializeField]
		protected float firstMaxDealy = 0;

		[SerializeField, Space]
		protected float minDelay = 0;
		[SerializeField]
		protected float maxDealy = 0;

		[SerializeField, Space]
		private float fadeInTime = 0;
		protected float estimatedFadeInTime {
			get { return (isPlaying && currentSound.TimeToEnd <= fadeInTime) ? currentSound.TimeToEnd / 2 : fadeInTime; }
		}
		[SerializeField]
		private float fadeOutTime = 0;
		protected float estimatedFadeOutTime {
			get { return (isPlaying && currentSound.TimeToEnd <= fadeOutTime) ? currentSound.TimeToEnd : fadeOutTime; }
		}

		protected bool isPlaying { get { return (currentSound != null && currentSound.IsPlaying); } }
		protected bool isPaused { get { return (currentSound != null && currentSound.IsPaused); } }
		[SerializeField, Space]
		private bool playOnStart = true;

		[SerializeField]
		protected PlayMode playMode = PlayMode.Default;
		public enum PlayMode
		{
			Default,
			LoopOne,
			LoopAll,
			Random

		}

		[SerializeField]
		protected SwitchPlayListMode switchPlayListMode = SwitchPlayListMode.AtOnce;
		public enum SwitchPlayListMode
		{
			WaitCurrentEnd,
			AtOnce

		}

		[SerializeField, Space, SoundName]
		private List<string> currentPlayList = new List<string>();
		public string[] CurrentPlayList {
			get {
				return currentPlayList.ToArray();
			}
			set {
				currentPlayList = new List<string>(value);
				playListPos = -1;
			}
		}

		public void SwitchToPlayList(string[] playlist, float fadeTime)
		{
			switchToPlayList(playlist, fadeTime);
		}

		protected int playListPos = -1;

		private Sound newPlayListSnd = null;
		private int[] privateHistoty = null;
		protected int[] history {
			get {
				if (privateHistoty == null) {
					privateHistoty = new int[20];
					for (int i = 0; i < privateHistoty.Length; i++) privateHistoty[i] = -1;
				}
				return privateHistoty;
			}
		}
		protected int historyPos = 19;

		protected virtual IEnumerator Start()
		{
			yield return new WaitForSeconds(Dice.Throw(firstMinDelay, firstMaxDealy));
			if (playOnStart) Play();
		}
		protected virtual void OnDestroy()
		{
			Stop();
		}

		public void Play()
		{
			if (isPlaying && isPaused) {
				currentSound.Play();
				Action<Sound> onFadeEnd = (sound) =>
				{
					if (this != null) StartWaitForFadeoutTime(estimatedFadeOutTime, true, PlayNext);
				};
				SoundManager.Instance.Mixer.Fade(currentSound, 0, currentSound.Volume, estimatedFadeInTime, onFadeEnd);
			}
			else if (!isPlaying) {
				PlayNext();
			}
		}

		public void PlayNext()
		{
			if (isPlaying) {
				Stop(PlayNext);
				return;
			}

			string nextTrack;
			if (getNextTrack(out nextTrack)) {
				currentSound = startTrack(nextTrack);
				Action<Sound> onFadeEnd = (sound) =>
				{
					if (this != null) {
						StartWaitForFadeoutTime(estimatedFadeOutTime, true, PlayNext);
					}
				};
				SoundManager.Instance.Mixer.Fade(currentSound, 0, currentSound.Volume, estimatedFadeInTime, onFadeEnd);
			}
		}

		public void PlayPrevious()
		{
			if (isPlaying) {
				Stop(PlayPrevious);
				return;
			}

			string nextTrack = getPreviousTrack();
			currentSound = startTrack(nextTrack);
			Action<Sound> onFadeEnd = (sound) =>
			{
				if (this != null) StartWaitForFadeoutTime(estimatedFadeOutTime, true, PlayNext);
			};
			SoundManager.Instance.Mixer.Fade(currentSound, 0, currentSound.Volume, estimatedFadeInTime, onFadeEnd);
		}

		public void Pause(Action onPause = null)
		{
			if (!isPlaying || isPaused) return;

			StopWaitForFadeoutTime();
			float volume = currentSound.Volume;
			Action<Sound> onFadeEnd = (sound) =>
			{
				sound.Pause();
				sound.Volume = volume;
				if (onPause != null) onPause();
			};
			SoundManager.Instance.Mixer.Fade(currentSound, currentSound.Volume, 0, estimatedFadeOutTime, onFadeEnd);
		}

		public void Stop(Action onStop = null)
		{
			if (!isPlaying) {
				if (onStop != null) onStop();
				return;
			}

			if (isPaused) {
				currentSound.Stop();
				if (onStop != null) onStop();
			}
			else {
				Action<Sound> onFadeEnd = (sound) =>
				{
					sound.Stop();
					if (onStop != null) onStop();
				};
				SoundManager.Instance.Mixer.Fade(currentSound, currentSound.Volume, 0, estimatedFadeOutTime, onFadeEnd);
			}
		}

		protected IEnumerator waitForFadeoutTimeHolder = null;
		protected void StartWaitForFadeoutTime(float fadeOutTime, bool makePause, Action onEnd)
		{
			if (waitForFadeoutTimeHolder != null) StopWaitForFadeoutTime();

			waitForFadeoutTimeHolder = waitForFadeoutTime(fadeOutTime, makePause, onEnd);
			StartCoroutine(waitForFadeoutTimeHolder);
		}
		protected void StopWaitForFadeoutTime()
		{
			if (waitForFadeoutTimeHolder != null) {
				StopCoroutine(waitForFadeoutTimeHolder);
				waitForFadeoutTimeHolder = null;
			}
		}

		private IEnumerator waitForFadeoutTime(float fadeOutTime, bool makePause, Action onEnd)
		{
			while (isPlaying && (currentSound.TimeToEnd > fadeOutTime || currentSound.Looped)) {
				yield return new WaitForEndOfFrame();
			}

			Stop();
			if (makePause) {
				float breakTime = Time.unscaledTime + Dice.Throw(minDelay, maxDealy);
				while (Time.unscaledTime <= breakTime) {
					yield return new WaitForEndOfFrame();
				}
			}
			onEnd();
			waitForFadeoutTimeHolder = null;
		}

		protected Sound startTrack(string trackName)
		{
			Sound snd = SoundManager.Instance.PlaySound(trackName);
			if (!allowLoopedSounds) snd.Looped = false;
			return snd;
		}

		protected string getPreviousTrack()
		{
			return currentPlayList[historyGetPrevious()];
		}

		protected bool getNextTrack(out string trackName)
		{
			if (historyPos < history.Length - 1) {
				trackName = currentPlayList[historyGetNext()];
				return true;
			}

			historyAdd(playListPos);
			if (currentPlayList.Count > 0) {
				switch (playMode) {
					case PlayMode.LoopOne:
						trackName = currentPlayList[playListPos = (playListPos >= 0) ? playListPos : 0];
						return true;

					case PlayMode.LoopAll:
						playListPos = (playListPos < currentPlayList.Count - 1) ? ++playListPos : 0;
						trackName = currentPlayList[playListPos];
						return true;

					case PlayMode.Default:
						if (playListPos < currentPlayList.Count - 1) {
							trackName = currentPlayList[++playListPos];
							return true;
						}
						break;

					case PlayMode.Random:
						playListPos = Dice.Throw(0, currentPlayList.Count - 1);
						trackName = currentPlayList[playListPos];
						return true;
				}
			}
			trackName = null;
			return false;
		}

		protected void historyAdd(int trackNo)
		{
			if (history.Length < 2 || trackNo < 0) return;

			for (int i = 1; i < history.Length; i++) {
				history[i - 1] = history[i];
			}
			history[history.Length - 1] = trackNo;
		}

		protected int historyGetNext()
		{
			if (history.Length < 2) return playListPos;

			if (historyPos < history.Length - 1) {
				historyPos++;
			}
			return history[historyPos];
		}

		protected int historyGetPrevious()
		{
			if (history.Length < 2) return playListPos;

			if (historyPos > 0 && history[historyPos - 1] < 0) {
				historyPos--;
			}
			return history[historyPos];
		}

		private void switchToPlayList(string[] playList, float fadeTime)
		{
			CurrentPlayList = playList;
			string nextTrack;
			if (isPlaying) {
				if (getNextTrack(out nextTrack)) {
					if (SoundManager.Instance.Mixer.IsFading(newPlayListSnd)) {
						SoundManager.Instance.Mixer.BreakFading(newPlayListSnd);
						SoundManager.Instance.Mixer.BreakFading(currentSound);
						SoundManager.Instance.Mixer.Fade(currentSound, currentSound.Volume, 0, fadeTime, (sound) =>
						{
							sound.Stop();
						});
						currentSound = newPlayListSnd;
					}

					StopWaitForFadeoutTime();
					if (switchPlayListMode == SwitchPlayListMode.AtOnce) {

						newPlayListSnd = startTrack(nextTrack);
						SoundManager.Instance.Mixer.Fade(newPlayListSnd, 0, newPlayListSnd.Volume, fadeTime);

						Action<Sound> onFadeOutEnd = (sound) =>
						{
							sound.Stop();
							currentSound = newPlayListSnd;
							StartWaitForFadeoutTime(estimatedFadeOutTime, true, PlayNext);
						};
						SoundManager.Instance.Mixer.Fade(currentSound, currentSound.Volume, 0, fadeTime, onFadeOutEnd);
					}
					else {
						Action onWaitEnd = () =>
						{
							SoundManager.Instance.Mixer.Fade(currentSound, currentSound.Volume, 0, fadeTime);
							currentSound = startTrack(nextTrack);
							SoundManager.Instance.Mixer.Fade(currentSound, 0, currentSound.Volume, fadeTime);
						};

						StartWaitForFadeoutTime(fadeTime, false, onWaitEnd);
						currentSound.Looped = false;
					}
				}
			}
			else Play();
		}
	}
}