﻿using UnityEngine;
using UnityEngine.UI;

namespace YOZH.Plugins.SoundManager
{
	[RequireComponent(typeof(Toggle))]
	public class ToggleSound : MonoBehaviour
	{
		[SerializeField]
		private string onSoundName = "touch";

		[SerializeField]
		private string offSoundName = "";

		private Toggle thisToggle = null;
		public Toggle ThisToggle {
			get {
				if (thisToggle == null)
					thisToggle = GetComponent<Toggle>();
				return thisToggle;
			}
		}

		private void Awake()
		{
			ThisToggle.onValueChanged.AddListener(onValueChanged);
		}

		private void OnDestroy()
		{
			thisToggle.onValueChanged.RemoveListener(onValueChanged);
		}

		private void onValueChanged(bool newState)
		{
			if (newState) {
				SoundManager.Instance.PlaySound(onSoundName);
			}
			else {
				SoundManager.Instance.PlaySound(offSoundName);
			}
		}
	}
}