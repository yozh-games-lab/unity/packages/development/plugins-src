﻿using UnityEngine;
using System.Collections;
using YOZH.Plugins.WindowManager;


namespace YOZH.Plugins.SoundManager
{
	[RequireComponent(typeof(Window))]
	public class WindowSoundAgent : MonoBehaviour
	{
		[SerializeField]
		private SoundEvent onSelect = new SoundEvent();

		[SerializeField, Space]
		private SoundEvent onBeforeOpen = new SoundEvent();
		[SerializeField]
		private SoundEvent onAfterOpen = new SoundEvent();

		[SerializeField, Space]
		private SoundEvent onBeforeClose = new SoundEvent();
		[SerializeField]
		private SoundEvent onAfterClose = new SoundEvent();

		[SerializeField, Space]
		private SoundEvent onBeforeShow = new SoundEvent();
		[SerializeField]
		private SoundEvent onAfterShow = new SoundEvent();

		[SerializeField, Space]
		private SoundEvent onBeforeHide = new SoundEvent();
		[SerializeField]
		private SoundEvent onAfterHide = new SoundEvent();

		private Window thisWindow = null;

		private void Awake()
		{
			thisWindow = GetComponent<Window>();

			Window.OnSelect += onSelectWin;

			Window.OnBeforeOpen += onBeforeOpenWin;
			Window.OnAfterOpen += onAfterOpenWin;

			Window.OnBeforeClose += onBeforeCloseWin;
			Window.OnAfterClose += onAfterCloseWin;

			Window.OnBeforeShow += onBeforeShowWin;
			Window.OnAfterShow += onAfterShowWin;

			Window.OnBeforeHide += onBeforeHideWin;
			Window.OnAfterHide += onAfterHideWin;
		}

		private void OnDestroy()
		{
			Window.OnSelect -= onSelectWin;

			Window.OnBeforeOpen -= onBeforeOpenWin;
			Window.OnAfterOpen -= onAfterOpenWin;

			Window.OnBeforeClose -= onBeforeCloseWin;
			Window.OnAfterClose -= onAfterCloseWin;

			Window.OnBeforeShow -= onBeforeShowWin;
			Window.OnAfterShow -= onAfterShowWin;

			Window.OnBeforeHide -= onBeforeHideWin;
			Window.OnAfterHide -= onAfterHideWin;
		}

		private void onSelectWin(Window win)
		{
			if (win == thisWindow && onSelect.enabled) {
				SoundManager.Instance.PlaySound(onSelect.sound);
			}
		}

		private void onBeforeOpenWin(Window win)
		{
			if (win == thisWindow && onBeforeOpen.enabled) {
				SoundManager.Instance.PlaySound(onBeforeOpen.sound);
			}
		}
		private void onAfterOpenWin(Window win)
		{
			if (win == thisWindow && onAfterOpen.enabled) {
				SoundManager.Instance.PlaySound(onAfterOpen.sound);
			}
		}

		private void onBeforeCloseWin(Window win)
		{
			if (win == thisWindow && onBeforeClose.enabled) {
				SoundManager.Instance.PlaySound(onBeforeClose.sound);
			}
		}
		private void onAfterCloseWin(Window win)
		{
			if (win == thisWindow && onAfterClose.enabled) {
				SoundManager.Instance.PlaySound(onAfterClose.sound);
			}
		}

		private void onBeforeShowWin(Window win)
		{
			if (win == thisWindow && onBeforeShow.enabled) {
				SoundManager.Instance.PlaySound(onBeforeShow.sound);
			}
		}
		private void onAfterShowWin(Window win)
		{
			if (win == thisWindow && onAfterShow.enabled) {
				SoundManager.Instance.PlaySound(onAfterShow.sound);
			}
		}

		private void onBeforeHideWin(Window win)
		{
			if (win == thisWindow && onBeforeHide.enabled) {
				SoundManager.Instance.PlaySound(onBeforeHide.sound);
			}
		}
		private void onAfterHideWin(Window win)
		{
			if (win == thisWindow && onAfterHide.enabled) {
				SoundManager.Instance.PlaySound(onAfterHide.sound);
			}
		}
	}
}