﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


namespace YOZH.Plugins.SoundManager
{
	public class UISoundAgent : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler
	{
		[SerializeField]
		private SoundEvent onClick = new SoundEvent();

		[SerializeField]
		private SoundEvent onPointerEnter = new SoundEvent();

		#region IPointerClickHandler implementation
		public void OnPointerClick(PointerEventData eventData)
		{
			if (onClick.enabled) SoundManager.Instance.PlaySound(onClick.sound);
		}
		#endregion

		#region ISelectHandler implementation
		public void OnPointerEnter(PointerEventData eventData)
		{
			if (onPointerEnter.enabled) SoundManager.Instance.PlaySound(onPointerEnter.sound);
		}
		#endregion
	}
}