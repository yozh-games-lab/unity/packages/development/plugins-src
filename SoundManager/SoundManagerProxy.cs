using UnityEngine;

namespace YOZH.Plugins.SoundManager
{
	public class SoundManagerProxy : MonoBehaviour
	{
		#region PublicFuncs
		public void PlaySound(string soundName)
		{
			SoundManager.Instance.PlaySound(soundName);
		}
		public void StopAllSoundsByName(string soundName)
		{
			SoundManager.Instance.StopAllSoundsByName(soundName);
		}
		#endregion
	}
}