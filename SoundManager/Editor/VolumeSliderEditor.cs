﻿using UnityEditor;

namespace YOZH.Plugins.SoundManager.Editor
{
	[CustomEditor(typeof(VolumeSlider)), CanEditMultipleObjects]
	public class VolumeSliderEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (AudioLibrary.Instance != null) {
				string[] voulumeGroupNames = AudioLibrary.Instance.GetVolumeGroupsNames();
				SerializedProperty volumeGroupName = serializedObject.FindProperty("volumeGroupName");
				drawPopup(volumeGroupName, voulumeGroupNames);
			}
		}

		private void drawPopup(SerializedProperty prop, string[] items)
		{
			int selectedIndex = 0;
			for (selectedIndex = items.Length - 1; selectedIndex > 0; selectedIndex--) {
				if (prop.stringValue == items[selectedIndex]) break;
			}
			prop.stringValue = items[EditorGUILayout.Popup(prop.displayName, selectedIndex, items)];
		}
	}
}