﻿using UnityEditor;
using UnityEngine;

namespace YOZH.Plugins.SoundManager.Editor
{
	[CustomEditor(typeof(Sound)), CanEditMultipleObjects]
	public class SoundEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{

			GUI.enabled = false;
			EditorGUILayout.PropertyField(serializedObject.FindProperty("m_Script"));

			Sound src = (Sound)target;
			if (src.IsPlaying) {
				EditorGUILayout.LabelField(string.Format("Total Time: {0}", src.Length));
				EditorGUILayout.Slider("Playing Time:", src.Time, 0, src.Length);
				EditorGUILayout.LabelField(string.Format("Rest Time: {0}", src.TimeToEnd));
				EditorGUILayout.Space();
				Repaint();
			}
			GUI.enabled = true;

			DrawPropertiesExcluding(serializedObject, "m_Script");
		}
	}
}