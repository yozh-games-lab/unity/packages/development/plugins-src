using UnityEditor;
using UnityEngine;

namespace YOZH.Plugins.SoundManager.Editor
{
	[CustomEditor(typeof(SoundMixer))]
	public class SoundMixerEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
			SoundMixer mixer = (SoundMixer)target;

			EditorGUILayout.Space();
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Total Volume");
			mixer.Volume = EditorGUILayout.Slider(mixer.Volume, 0, 1);
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Space();

			if (AudioLibrary.Instance != null) {
				GUI.enabled = Application.isPlaying;
				for (int i = 0; i < AudioLibrary.Instance.VolumeGroups.Length; i++) {
					VolumeGroup vg = (VolumeGroup)AudioLibrary.Instance.VolumeGroups[i];
					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.LabelField(vg.Name);
					float newVol = EditorGUILayout.Slider(mixer.GetVolume(vg.Name), 0, 1);
					mixer.SetVolume(vg.Name, newVol);
					EditorGUILayout.EndHorizontal();
				}
				GUI.enabled = true;
			}
		}
	}
}