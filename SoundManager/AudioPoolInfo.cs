namespace YOZH.Plugins.SoundManager
{
	[System.Serializable]
	public class AudioPoolInfo
	{
		public string PoolName = typeof(AudioPool).ToString();
		public int MaxCount = 10;
		public AudioPool.OverFitMode OverFitMode = AudioPool.OverFitMode.Override;
		public bool DestroyOnLoad = true;
		public bool FillAtStart = false;

		///<summary>
		/// This will create a new gameObject
		/// </summary>
		public static explicit operator AudioPool(AudioPoolInfo poolInfo)
		{
			return AudioPool.Create(poolInfo);
		}
	}
}