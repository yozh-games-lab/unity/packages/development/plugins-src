using System.Collections.Generic;
using UnityEngine;

namespace YOZH.Plugins.SoundManager
{
	public class AudioPool : MonoBehaviour
	{
		#region Variables
		public enum OverFitMode
		{
			Extend,
			Override
		}

		public OverFitMode CurrentOverFitMode;
		[SerializeField]
		private int maxCount;
		public int currentMaxCount {
			get { return maxCount; }
		}

		public int DefaultMaxCount { get; private set; }
		public bool DestroyOnLoad;

		private List<Sound> sounds = new List<Sound>(0);
		private static AudioPoolInfo defultSetup = new AudioPoolInfo();
		#endregion

		#region Events
		private void Awake()
		{
			setup(defultSetup);
		}
		#endregion

		#region PublicFuncs
		public static AudioPool Create(AudioPoolInfo poolInfo)
		{
			AudioPool result = CreateEmpty();
			result.setup(poolInfo);
			return result;
		}

		public void Destroy()
		{
			Destroy(gameObject);
		}

		public void Optimize()
		{
			optimizePool();
		}

		public static AudioPool CreateEmpty()
		{
			GameObject go = new GameObject(typeof(AudioPool).ToString());
			return go.AddComponent<AudioPool>();
		}

		public Sound ReserveSound()
		{
			Sound result = getSound();
			reserveSound(result);
			return result;
		}

		public void ReleaseSound(Sound sound)
		{
			releaseSound(sound);
		}
		#endregion

		#region Private funcs
		private Sound getSound()
		{
			Sound result = findFreeSound();
			if (result == null) {
				switch (CurrentOverFitMode) {
					case OverFitMode.Extend:
						result = extendPool();
						break;
					case OverFitMode.Override:
						result = findEndClosestSound();
						break;
				}
			}
			return result;
		}

		private void setup(AudioPoolInfo poolInfo)
		{
			this.name = poolInfo.PoolName;
			CurrentOverFitMode = poolInfo.OverFitMode;
			DefaultMaxCount = poolInfo.MaxCount;
			maxCount = poolInfo.MaxCount;
			DestroyOnLoad = poolInfo.DestroyOnLoad;
			if (poolInfo.FillAtStart) {
				fillPool();
			}
		}

		private Sound findFreeSound()
		{
			foreach (Sound sound in sounds) {
				if (!sound.IsPlaying && !sound.Reserved) {
					return sound;
				}
			}
			return null;
		}

		private Sound findEndClosestSound()
		{
			if (sounds.Count == 0)
				return addNewEmptySound();
			int iResult = 0;
			for (int i = 0; i < sounds.Count; i++) {
				if (sounds[i].IsPlaying && sounds[i].TimeToEnd < sounds[iResult].TimeToEnd)
					iResult = i;
			}
			sounds[iResult].Stop();
			return sounds[iResult];
		}

		private void fillPool()
		{
			if (sounds.Count < DefaultMaxCount) {
				while (sounds.Count <= DefaultMaxCount) {
					addNewEmptySound();
				}
			}
			else {
				optimizePool();
			}
		}

		private void clearPool()
		{
			for (int i = sounds.Count - 1; i >= 0; i--) {
				deleteSound(sounds[i]);
			}
		}

		private void optimizePool()
		{
			maxCount = DefaultMaxCount;

			for (int i = sounds.Count - 1; i >= 0; i--) {
				if (sounds.Count > DefaultMaxCount) {
					if (!sounds[i].IsPlaying) {
						deleteSound(sounds[i]);
					}
				}
				else return;
			}

			while (sounds.Count > DefaultMaxCount) {
				int iRemove = 0;
				for (int i = 1; i < sounds.Count; i++) {
					if (sounds[i].TimeToEnd < sounds[iRemove].TimeToEnd) {
						iRemove = i;
					}
				}
				deleteSound(sounds[iRemove]);
			}
		}

		private Sound addNewEmptySound()
		{
			Sound sound = Sound.CreateEmpty();
			sound.transform.SetParent(transform);
			sounds.Add(sound);
			return sound;
		}

		private Sound addNewSound(SoundInfo soundInfo)
		{
			Sound sound = addNewEmptySound();
			sound.Setup(soundInfo);
			return sound;
		}

		private void registerSound(Sound sound)
		{
			if (sound == null) {
				Debug.LogError("[" + GetType().Name + "] Null in registerSound methode");
				return;
			}

			if (sounds.Count < maxCount) {
				sounds.Add(sound);
			}
			else {
				Sound oldSound = findFreeSound();
				if (oldSound == null) {
					switch (CurrentOverFitMode) {
						case OverFitMode.Extend:
							extendPool(sound);
							return;
						case OverFitMode.Override:
							oldSound = findEndClosestSound();
							replaceSound(oldSound, sound);
							return;
					}
				}
				else {
					replaceSound(oldSound, sound);
				}
			}
		}

		private void unregisterSound(Sound sound)
		{
			if (sounds.Contains(sound)) sounds.Remove(sound);
		}

		private Sound extendPool()
		{
			maxCount++;
			return addNewEmptySound();
		}

		private Sound extendPool(SoundInfo soundInfo)
		{
			maxCount++;
			return addNewSound(soundInfo);
		}

		private void extendPool(Sound sound)
		{
			maxCount++;
			sounds.Add(sound);
		}

		private void replaceSound(Sound oldSound, Sound newSound)
		{
			deleteSound(oldSound);
			sounds.Add(newSound);
		}

		private void deleteSound(Sound sound)
		{
			sounds.Remove(sound);
			Destroy(sound.gameObject);
		}

		private void reserveSound(Sound sound)
		{
			sound.Reserved = true;
		}

		private void releaseSound(Sound sound)
		{
			sound.Reserved = false;
		}
		#endregion
	}
}