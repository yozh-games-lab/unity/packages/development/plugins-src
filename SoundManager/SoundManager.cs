using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace YOZH.Plugins.SoundManager
{
	[RequireComponent(typeof(SoundMixer))]
	public class SoundManager : MonoBehaviour
	{
		#region Constants
		public const string RESOURCE_PATH = "GameSettings/SoundManager";
		#endregion

		#region Singletone
		private static SoundManager instance = null;
		public static SoundManager Instance {
			get {
				if (instance == null) {
					GameObject go = new GameObject();
					instance = go.AddComponent<SoundManager>();
					go.name = instance.GetType().Name;
				}
				return instance;
			}
		}
		private void awakeInit()
		{
			DontDestroyOnLoad(gameObject);
			if (instance == null) {
				name = typeof(SoundManager).ToString();
				instance = this;
			}
			else {
				Destroy(this);
				Destroy(Mixer);
			}
		}
		#endregion

		#region Variables
		private SoundMixer mixer = null;
		public SoundMixer Mixer {
			get {
				if (mixer == null) cacheMixer();
				return mixer;
			}
		}
		private List<Sound> reservedSounds = new List<Sound>();
		private Dictionary<string, AudioPool> pools = new Dictionary<string, AudioPool>();
		private float previousTimeScale = -1;
		#endregion

		#region Events
		void Awake()
		{
			SceneManager.sceneLoaded += onSceneLoaded;
			awakeInit();
			cacheMixer();
		}
		void Start()
		{
			_riseOnTimeScaleChanged();
		}
		void Update()
		{
			if (Time.timeScale != previousTimeScale) {
				_riseOnTimeScaleChanged();
			}
		}
		void OnDestroy()
		{
			SceneManager.sceneLoaded -= onSceneLoaded;
		}

		private static EventHandler onTimeScaleChanged;
		public static event EventHandler OnTimeScaleChanged {
			add {
				if (onTimeScaleChanged != null) {
					Delegate[] delegates = onTimeScaleChanged.GetInvocationList();
					for (int i = 0; i < delegates.Length; i++) if (delegates[i] == (Delegate)value) return;
				}
				onTimeScaleChanged += value;
			}
			remove { onTimeScaleChanged -= value; }
		}

		private void _riseOnTimeScaleChanged()
		{
			previousTimeScale = Time.timeScale;
			if (onTimeScaleChanged != null) onTimeScaleChanged(this, new EventArgs());
		}

		public static void RegisterVolumeCallback(string volumeGroupName, EventHandler callback)
		{
			VolumeGroup group = Instance.Mixer.GetVolumeGroup(volumeGroupName);
			RegisterVolumeCallback(group, callback);
		}

		public static void RegisterVolumeCallback(VolumeGroup volumeGroup, EventHandler callback)
		{
			Instance.Mixer.OnTotalVolumeChanged += callback;
			if (volumeGroup != null) {
				volumeGroup.OnVolumeChanged += callback;
			}
		}

		public static void UnregisterVolumeCallback(string volumeGroupName, EventHandler callback)
		{
			VolumeGroup group = instance.Mixer.GetVolumeGroup(volumeGroupName);
			UnregisterVolumeCallback(group, callback);
		}

		public static void UnregisterVolumeCallback(VolumeGroup volumeGroup, EventHandler callback)
		{
			if (instance != null && instance.Mixer != null) {
				instance.Mixer.OnTotalVolumeChanged -= callback;
				if (volumeGroup != null) {
					volumeGroup.OnVolumeChanged -= callback;
				}
			}
		}
		#endregion

		#region PublicFuncs
		/// <summary>
		/// Plays the sound by sound name.
		/// </summary>
		/// <returns>The sound.</returns>
		/// <param name="soundName">Sound Name.</param>
		public Sound PlaySound(string soundName)
		{
			if (string.IsNullOrEmpty(soundName)) return null;
			Sound sound = reserveSound(soundName);
			if (sound != null) {
				sound.Play();
				sound.OnStop += releaseSound;
			}
			return sound;
		}
		/// <summary>
		/// Stops playing all sounds by name.
		/// </summary>
		/// <param name="soundName">Sound Name.</param>
		public void StopAllSoundsByName(string soundName)
		{
			foreach (Sound sound in reservedSounds) {
				if (sound.name == soundName && sound.IsPlaying) sound.Stop();
			}
		}
		/// <summary>
		/// Reserves the sound and makes it get out from pool until being released.
		/// </summary>
		/// <returns>The sound.</returns>
		/// <param name="soundName">Sound Name.</param>
		public Sound ReserveSound(string soundName)
		{
			return reserveSound(soundName);
		}
		/// <summary>
		/// Releases the sound and makes it return to pool so it can be used in other case.
		/// </summary>
		/// <param name="snd">Snd.</param>
		public void ReleaseSound(Sound snd)
		{
			releaseSound(snd);
		}
		#endregion

		#region PrivateFuncs
		void onSceneLoaded(Scene arg0, LoadSceneMode arg1)
		{
			optimize();
		}
		private void optimize()
		{
			optimizePools();
		}
		private void optimizePools()
		{
			AudioPool[] pools = new AudioPool[this.pools.Count];
			this.pools.Values.CopyTo(pools, 0);
			foreach (AudioPool pool in pools) {
				if (pool.DestroyOnLoad) {
					pool.Destroy();
					this.pools.Remove(pool.name);
				}
				else {
					pool.Optimize();
				}
			}
		}
		private void cacheMixer()
		{
			mixer = GetComponent<SoundMixer>();
		}

		private Sound reserveSound(string soundName)
		{
			return reserveSound(AudioLibrary.Instance.GetSoundInfo(soundName));
		}

		private Sound reserveSound(SoundInfo soundInfo)
		{
			if (soundInfo == null) return null;
			Sound sound = null;
			AudioPool pool = findPool(soundInfo.PoolName);
			if (pool != null) sound = pool.ReserveSound();

			if (sound != null) {
				sound.Setup(soundInfo);
				if (!reservedSounds.Contains(sound)) reservedSounds.Add(sound);
				sound.OnDestroyEvent += delegate (Sound snd)
				{
					if (reservedSounds.Contains(snd)) reservedSounds.Remove(snd);
				};
			}
			return sound;
		}

		private void releaseSound(Sound sound)
		{
			if (sound == null) return;
			AudioPool pool = findPool(sound.poolName);
			if (pool == null) return;
			pool.ReleaseSound(sound);
			if (reservedSounds.Contains(sound)) reservedSounds.Remove(sound);
			sound.OnStop -= releaseSound;
		}

		private AudioPool findPool(string poolName)
		{
			AudioPool pool = null;
			if (!pools.TryGetValue(poolName, out pool)) {
				pool = getNewPool(poolName);
				if (pool != null) pools.Add(poolName, pool);
			}
			return pool;
		}

		private AudioPool getNewPool(string poolName)
		{
			AudioPool pool = AudioLibrary.Instance.GetPoolInfo(poolName);
			if (pool != null) pool.transform.SetParent(transform);
			return pool;
		}
		#endregion
	}
}