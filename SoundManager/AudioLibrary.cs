using System.Collections.Generic;
using UnityEngine;


namespace YOZH.Plugins.SoundManager
{
	public class AudioLibrary : ScriptableObject
	{
		#region Constants
		public const string RESOURCE_PATH = "AudioLibrary";
		#endregion

		#region Singletone
		private static AudioLibrary instance = null;
		public static AudioLibrary Instance {
			get {
				if (instance == null) {
					instance = Resources.Load<AudioLibrary>(Core.Misc.MiscUtilities.NormalizePath(string.Format("{0}/{1}", SoundManager.RESOURCE_PATH, RESOURCE_PATH)));
					if (instance != null) instance.Init();
				}
				return instance;
			}
		}
		public static bool Initialized { get; private set; }
		public void Init()
		{
			if (Initialized) return;
			fillPoolsDict();
			fillSoundsDict();
			Initialized = true;
		}
		#endregion

		#region Variables
		private Dictionary<string, AudioPoolInfo> poolsDict = null;
		private Dictionary<string, SoundInfo> soundsDict = null;

		[SerializeField]
		private bool storeVolumeInPlayerPrefs = true;

		public bool StoreVolumeInPlayerPrefs {
			get {
				return storeVolumeInPlayerPrefs;
			}
		}

		[SerializeField]
		private VolumeGroupInfo[] volumeGroups = new VolumeGroupInfo[1];

		public VolumeGroupInfo[] VolumeGroups {
			get {
				return volumeGroups.Clone() as VolumeGroupInfo[];
			}
		}

		[SerializeField]
		private AudioPoolInfo[] pools = new AudioPoolInfo[1];

		public AudioPoolInfo[] Pools {
			get {
				return pools.Clone() as AudioPoolInfo[];
			}
		}

		[SerializeField]
		private SoundInfo[] sounds = new SoundInfo[1];

		public SoundInfo[] Sounds {
			get {
				return sounds.Clone() as SoundInfo[];
			}
		}
		#endregion

		#region PublicFuncs
		/// <summary>
		/// Returns the audio pool info buy pool name.
		/// </summary>
		/// <returns>The pool.</returns>
		/// <param name="poolName">Pool Name.</param>
		public AudioPool GetPoolInfo(string poolName)
		{
			if (poolsDict == null) fillPoolsDict();

			AudioPoolInfo result = null;
			poolsDict.TryGetValue(poolName, out result);
			return (AudioPool)result;
		}

		/// <summary>
		/// Returns the sound info by sound name.
		/// </summary>
		/// <returns>The sound info.</returns>
		/// <param name="soundName">Sound Name.</param>
		public SoundInfo GetSoundInfo(string soundName)
		{
			if (soundsDict == null) fillSoundsDict();

			SoundInfo result = null;
			soundsDict.TryGetValue(soundName, out result);
			return result;
		}
		/// <summary>
		/// Returns an array of Pools names.
		/// </summary>
		/// <returns>An array of Pools names.</returns>
		public string[] GetPoolsNames()
		{
			string[] result = new string[pools.Length];
			for (int i = 0; i < pools.Length; i++) {
				result[i] = pools[i].PoolName;
			}
			return result;
		}
		/// <summary>
		/// Returns an array of Sounds names.
		/// </summary>
		/// <returns>An array of Sounds names.</returns>.
		public string[] GetSoundsNames()
		{
			string[] result = new string[sounds.Length];
			for (int i = 0; i < sounds.Length; i++) {
				result[i] = sounds[i].SoundName;
			}
			return result;
		}
		/// <summary>
		/// Returns an array of Volume groups names.
		/// </summary>
		/// <returns>An array of Volume groups names.</returns>
		public string[] GetVolumeGroupsNames()
		{
			string[] result = new string[volumeGroups.Length];
			for (int i = 0; i < volumeGroups.Length; i++) {
				result[i] = volumeGroups[i].Name;
			}
			return result;
		}
		#endregion

		#region PrivateFuncs
		private void fillPoolsDict()
		{
			poolsDict = new Dictionary<string, AudioPoolInfo>();
			foreach (AudioPoolInfo pool in pools) {
				if (!poolsDict.ContainsKey(pool.PoolName)) poolsDict.Add(pool.PoolName, pool);
			}
		}
		private void fillSoundsDict()
		{
			soundsDict = new Dictionary<string, SoundInfo>();
			foreach (SoundInfo sound in sounds) {
				if (!soundsDict.ContainsKey(sound.SoundName)) soundsDict.Add(sound.SoundName, sound);
			}
		}
		#endregion
	}
}