﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace YOZH.Plugin.Pooling.PoolV1.Editor
{
	[CustomPropertyDrawer(typeof(PoolItemKeyPropertyAttribute))]
	public class PoolItemKeyPropertyDrawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			List<string> characterKeys = PoolHub.Instance.PoolItemNames;

			if (characterKeys.Count > 0) {
				if (!characterKeys.Contains(property.stringValue)) {
					property.stringValue = characterKeys[0];
				}

				int index = EditorGUI.Popup(position, property.displayName, characterKeys.FindIndex(i => i == property.stringValue), characterKeys.ToArray());
				property.stringValue = characterKeys[index];
			}
			else {
				EditorGUI.HelpBox(position, string.Format("Please setup pools in {0}", typeof(PoolHub).Name), MessageType.Warning);
			}
		}
	}
}
