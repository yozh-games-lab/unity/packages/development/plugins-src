﻿using UnityEditor;
using YOZH.Core.Misc.Editor;

namespace YOZH.Plugin.Pooling.PoolV1.Editor
{
	public class PoolEditorMenu : UnityEditor.Editor
	{
		[MenuItem("HBG/Pool manager/Create pool hub")]
		[MenuItem("Assets/Create/New pool hub")]
		public static void NewAudioLibrary()
		{
			ScriptableObjectUtility.CreateScriptableObjectAtPath<PoolHub>(
				string.Format("HBG/Resources/{0}", PoolHub.RESOURCE_PATH), true);
		}
	}
}