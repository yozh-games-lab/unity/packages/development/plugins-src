﻿using YOZH.Core.Misc.Interfaces;

namespace YOZH.Plugin.Pooling.PoolV1
{
	public interface IPool : IGameObjectComponent
	{
		string SampleName { get; }
		IPoolable GetFromPool();
		bool TryGetFromPool(out IPoolable result);
		void Clear(bool soft = true);
		void Purge();
	}
}
