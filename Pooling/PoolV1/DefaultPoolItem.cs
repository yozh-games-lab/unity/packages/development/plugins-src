﻿using System;
using UnityEngine;


namespace YOZH.Plugin.Pooling.PoolV1
{
	public class DefaultPoolItem : MonoBehaviour, IPoolable
	{
		#region IPoolable implementation
		public bool Busy {
			get {
				return (gameObject.activeSelf) ? true : false;
			}
		}

		public GameObject GameObject { get { return gameObject; } }

		public object Clone()
		{
			DefaultPoolItem clone = Instantiate(this);
			clone.name = clone.name.Replace("(Clone)", "");
			return clone;
		}
		public void Dispose()
		{
			OnPoolIn = null;
			OnPoolOut = null;
			Destroy(gameObject);
		}
		public void PoolIn()
		{
			gameObject.SetActive(false);
			raiseOnPoolIn();
		}
		public void PoolOut()
		{
			gameObject.SetActive(true);
			raiseOnPoolOut();
		}

		public event IPoolableEvent OnPoolIn = (IPoolable sender) => { };
		private void raiseOnPoolIn()
		{
			onPoolInExtraJob();
			OnPoolIn(this);
		}
		public event IPoolableEvent OnPoolOut = (IPoolable sender) => { };
		private void raiseOnPoolOut()
		{
			onPoolOutExtraJob();
			OnPoolOut(this);
		}

		public T GetImplementation<T>() where T : Component
		{
			return this as T;
		}
		#endregion

		protected virtual void onPoolInExtraJob()
		{
		}
		protected virtual void onPoolOutExtraJob()
		{
		}

		public T FindComponent<T>() where T : Component
		{
			return GetComponent<T>();
		}
	}
}