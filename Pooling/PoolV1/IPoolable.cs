﻿using System;
using YOZH.Core.Misc.Interfaces;

namespace YOZH.Plugin.Pooling.PoolV1
{
	public interface IPoolable : ICloneable, IDisposable, IGameObjectComponent
	{
		bool Busy { get; }
		void PoolIn();
		void PoolOut();

		event IPoolableEvent OnPoolIn;
		event IPoolableEvent OnPoolOut;
	}

	public delegate void IPoolableEvent(IPoolable sender);
}
