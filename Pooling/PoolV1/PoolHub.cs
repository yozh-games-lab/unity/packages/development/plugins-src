﻿using System;
using System.Collections.Generic;
using UnityEngine;
using YOZH.Core.Misc.Interfaces;

namespace YOZH.Plugin.Pooling.PoolV1
{
	public class PoolHub : ScriptableObject
	{
		public const string RESOURCE_PATH = "GameSettings/PoolHub";
		private static PoolHub instance = null;
		public static PoolHub Instance {
			get {
				if (instance == null) {
					instance = Resources.Load<PoolHub>(RESOURCE_PATH);
				}
				return instance;
			}
		}

		private PoolHubContainer poolsContainerChache = null;
		private PoolHubContainer poolsContainer {
			get {
				if (poolsContainerChache == null) {
					GameObject poolsContainerGameObject = new GameObject(typeof(PoolHubContainer).Name, typeof(PoolHubContainer));
					poolsContainerChache = poolsContainerGameObject.GetComponent<PoolHubContainer>();
				}
				return poolsContainerChache;
			}
		}

		[SerializeField]
		private List<PoolMeta> poolsList = new List<PoolMeta>();


		public void Preload()
		{
			foreach (PoolMeta meta in poolsList) {
				if (meta.Preload) {
					if (!poolsContainer.ContainsKey(meta.PoolName)) {
						instantiatePool(meta);
					}
				}
			}
		}
		public void Clear(bool soft = true)
		{
			poolsContainer.Clear(soft);
		}
		public bool TryGetPoolItem(string poolKey, out IPoolable result)
		{
			if (string.IsNullOrEmpty(poolKey)) throw new ArgumentNullException("poolKey");

			IPool targetPool = null;
			if (!poolsContainer.TryGetValue(poolKey, out targetPool)) {
				PoolMeta meta = poolsList.Find(item => item.PoolName == poolKey);
				targetPool = instantiatePool(meta);
			}

			if (targetPool.TryGetFromPool(out result)) {
				result.FindComponent<Transform>().SetParent(poolsContainer.ItemsRoot);
				return true;
			}
			else return false;

		}
		public bool TryGetPoolItem<T>(string poolKey, out T result)
		{
			IPoolable poolItem = null;
			if (TryGetPoolItem(poolKey, out poolItem)) {
				result = poolItem.GameObject.GetComponent<T>();
				if (result == null) poolItem.PoolIn();
				return (result != null);
			}
			result = default(T);
			return false;
		}

		public bool TryGetPoolItemByName(string sampleName, out IPoolable result)
		{
			if (string.IsNullOrEmpty(sampleName)) throw new ArgumentNullException("sampleName");

			PoolMeta meta = poolsList.Find(item => item.SampleName == sampleName);
			return TryGetPoolItem(meta.PoolName ?? "", out result);
		}
		public bool TryGetPoolItemByName<T>(string sampleName, out T result) where T : IGameObjectComponent
		{
			if (string.IsNullOrEmpty(sampleName)) throw new ArgumentNullException("sampleName");

			PoolMeta meta = poolsList.Find(item => item.SampleName == sampleName);
			return TryGetPoolItem<T>(meta.PoolName ?? "", out result);
		}

		private IPool instantiatePool(PoolMeta meta)
		{
			if (meta == null) throw new ArgumentNullException("meta");

			GameObject instance = Instantiate(meta.Pool.GameObject);
			IPool result = instance.GetComponent<IPool>();
			result.FindComponent<Transform>().SetParent(poolsContainer.PoolsRoot);
			poolsContainer.Add(meta.PoolName, result);
			return result;
		}


#if UNITY_EDITOR
		[ContextMenu("Sort by pool")]
		private void sortByPoolName()
		{
			poolsList.Sort((PoolMeta itemA, PoolMeta itemB) =>
			{
				return string.Compare(itemA.PoolName, itemB.PoolName, StringComparison.Ordinal);
			});
		}
		[ContextMenu("Sort by sample")]
		private void sortBySampleName()
		{
			poolsList.Sort((PoolMeta itemA, PoolMeta itemB) =>
			{
				return string.Compare(itemA.SampleName, itemB.SampleName, StringComparison.Ordinal);
			});
		}
		[ContextMenu("Autofill")]
		private void Autofill()
		{
			poolsList.Clear();
			string[] GUIDs = UnityEditor.AssetDatabase.FindAssets("t:Prefab");
			foreach (string GUID in GUIDs) {

				string path = UnityEditor.AssetDatabase.GUIDToAssetPath(GUID);
				if (path.Contains(@"/Resources/")) {
					GameObject asset = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>(path);

					if (asset.GetComponent<IPool>() != null) {
						string rPath = System.IO.Path.GetFileNameWithoutExtension(path);
						string[] pathSplit = path.Split('/');

						if (pathSplit.Length > 1) {
							for (int index = pathSplit.Length - 2; (index >= 0 && pathSplit[index] != "Resources"); index--) {
								rPath = string.Format("{0}/{1}", pathSplit[index], rPath);
							}
						}
						poolsList.Add(new PoolMeta(rPath));
					}

				}
			}
		}
#endif

		public List<string> PoolNames {
			get {
				List<string> result = new List<string>();
				foreach (PoolMeta meta in poolsList) {
					if (!result.Contains(meta.PoolName)) {
						result.Add(meta.PoolName);
					}
				}
				result.Sort();
				return result;
			}
		}
		public List<string> PoolItemNames {
			get {
				List<string> result = new List<string>();
				foreach (PoolMeta meta in poolsList) {
					if (!result.Contains(meta.SampleName)) {
						result.Add(meta.SampleName);
					}
				}
				result.Sort();
				return result;
			}
		}
	}
}