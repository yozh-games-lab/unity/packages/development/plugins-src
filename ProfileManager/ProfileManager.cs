﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace YOZH.Plugins.ProfileManager
{
	public static class ProfileManager
	{
		public static string ProfilesDirectory {
			get {
				string result = Path.Combine(Application.persistentDataPath, "UserProfiles");
				if (!Directory.Exists(result)) {
					Directory.CreateDirectory(result);
				}

				return result;
			}
		}

		public static ProfileRecord[] GetUserRecords(string userName)
		{
			return getUserRecords().FindAll(record => record.UserName == userName).ToArray();
		}

		public static ProfileRecord[] GetUserRecords()
		{
			return getUserRecords().ToArray();
		}

		private static List<ProfileRecord> getUserRecords()
		{
			List<ProfileRecord> userProfileRecords = new List<ProfileRecord>();

			if (Directory.Exists(ProfilesDirectory)) {
				string[] files = Directory.GetFiles(ProfilesDirectory);
				for (int index = 0; index < files.Length; index++) {
					var fileName = Path.GetFileName(files[index]);
					if (!string.IsNullOrEmpty(fileName)) {
						ProfileRecord profileRecord = ProfileRecord.Load(ProfilesDirectory, fileName);
						if (profileRecord != null) userProfileRecords.Add(profileRecord);
					}
				}
			}

			return userProfileRecords;
		}
	}
}