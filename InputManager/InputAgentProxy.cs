﻿using UnityEngine;

namespace YOZH.Plugins.InputManager
{
	public class InputAgentProxy : MonoBehaviour
	{
		public void OnEscape()
		{
			InputManager.Instance.RaiseOnEscape();
		}
	}
}