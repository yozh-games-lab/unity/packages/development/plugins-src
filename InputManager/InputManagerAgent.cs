﻿using UnityEngine;

namespace YOZH.Plugins.InputManager
{
	public abstract class InputManagerAgent : MonoBehaviour
	{
		private static InputManagerAgent instance = null;
		public static T GetInstance<T>() where T : InputManagerAgent
		{
			if (instance == null) {
				GameObject go = new GameObject(typeof(T).ToString());
				instance = go.AddComponent<T>();
			}
			return instance as T;
		}

		protected virtual void Awake()
		{
			if (instance == null) instance = this;

			InputManager.OnEscape += onEscape;
			InputManager.OnClick += onClick;
			InputManager.OnClickDown += onClickDown;
			InputManager.OnClickUp += onClickUp;
			InputManager.OnDoubleClick += OnDoubleClick;
			InputManager.OnHoldStationaty += onHoldStationary;
			InputManager.OnHold += onHold;
			InputManager.OnSwipe += onSwipe;
		}

		protected virtual void Start()
		{

		}

		protected virtual void OnDestroy()
		{
			InputManager.OnEscape -= onEscape;
			InputManager.OnClick -= onClick;
			InputManager.OnClickDown -= onClickDown;
			InputManager.OnClickUp -= onClickUp;
			InputManager.OnDoubleClick -= OnDoubleClick;
			InputManager.OnHoldStationaty -= onHoldStationary;
			InputManager.OnHold -= onHold;
			InputManager.OnSwipe -= onSwipe;
		}

		protected virtual void onEscape() { }
		protected virtual void onClick(Vector2 screenPoint) { }
		protected virtual void onClickDown(Vector2 screenPoint) { }
		protected virtual void onClickUp(Vector2 screenPoint) { }
		protected virtual void onDoubleClick(Vector2 screenPoint) { }
		protected virtual void onSwipe(Vector2 startPoint, Vector2 endPoint) { }
		protected virtual void onHoldStationary(Vector2 screenPoint, float holdTime) { }
		protected virtual void onHold(Vector2 screenPoint, float holdTime) { }

		public void OnEscape()
		{
			onEscape();
		}
		public void OnClick(Vector2 screenPoint)
		{
			onClick(screenPoint);
		}
		public void OnClickDown(Vector2 screenPoint)
		{
			onClickDown(screenPoint);
		}
		public void OnClickUp(Vector2 screenPoint)
		{
			onClickUp(screenPoint);
		}
		public void OnDoubleClick(Vector2 screenPoint)
		{
			onDoubleClick(screenPoint);
		}
		public void OnSwipe(Vector2 startPoint, Vector2 endPoint)
		{
			onSwipe(startPoint, endPoint);
		}
		public void OnHoldStationary(Vector2 screenPoint, float holdTime)
		{
			onHoldStationary(screenPoint, holdTime);
		}
		public void OnHold(Vector2 screenPoint, float holdTime)
		{
			onHold(screenPoint, holdTime);
		}

	}
}