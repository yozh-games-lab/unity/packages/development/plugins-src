﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;


namespace YOZH.Plugins.InputManager
{
	[RequireComponent(typeof(RectTransform), typeof(Graphic))]
	public class Joystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
	{
		public static Joystick GetJoystick(string tag)
		{
			return InputManager.GetJoystick(tag);
		}

		public Vector2 Direction2 { get; private set; }
		public Vector3 Direction3 { get { return new Vector3(Direction2.x, 0, Direction2.y); } }
		public bool IsDown { get; private set; }
		public bool IsMoved { get; private set; }
		public float Power { get; private set; }

		[SerializeField]
		private float voidZoneRadius = 16;
		[SerializeField]
		private RectTransform voidZone = null;

		[SerializeField]
		private float joystickRadius = 100;
		[SerializeField]
		private RectTransform joystickZone = null;

		[SerializeField]
		private float pressZoneRadius = 48;
		[SerializeField]
		private RectTransform pressZone = null;

		private RectTransform thisRect = null;
		private Vector2 startPoint = Vector2.zero;
		private Vector2 pressPoint = Vector2.zero;

		private void Awake()
		{
			thisRect = GetComponent<RectTransform>();
			addJoystick();

			if (SystemInfo.deviceType != DeviceType.Handheld) {
				resetVisualization();
			}
		}

		private void OnDestroy()
		{
			removeJoyStick();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (!InputManager.Instance.IsTouchScreen) return;

			IsDown = true;
			IsMoved = false;
			startPoint = eventData.pressPosition;
			pressPoint = eventData.position;
			Direction2 = Vector2.zero;

			showVisualization();
		}
		public void OnDrag(PointerEventData eventData)
		{
			if (!InputManager.Instance.IsTouchScreen) {
				return;
			}

			pressPoint = eventData.position;
			if (!RectTransformUtility.RectangleContainsScreenPoint(thisRect, pressPoint)) {
				OnPointerUp(eventData);
				return;
			}

			float currentRadius = Vector2.Distance(pressPoint, startPoint);
			if (currentRadius >= voidZoneRadius) {
				IsMoved = true;
				if (currentRadius > joystickRadius) {
					startPoint += ((pressPoint - startPoint) - (pressPoint - startPoint).normalized * joystickRadius);
				}
				Power = Mathf.Clamp01(currentRadius / joystickRadius);
				Direction2 = (pressPoint - startPoint).normalized;
			}
			else {
				IsMoved = false;
			}

			showVisualization();
		}
		public void OnPointerUp(PointerEventData eventData)
		{
			if (!InputManager.Instance.IsTouchScreen) return;

			IsDown = false;
			IsMoved = false;

			Direction2 = Vector2.zero;
			startPoint = Vector2.zero;
			pressPoint = Vector2.zero;
			Power = 0;

			hideVisualization();
		}

		private void addJoystick()
		{
			InputManager.AddJoystick(tag, this);
		}
		private void removeJoyStick()
		{
			InputManager.RemoveJoyStick(tag);
		}

		private void resetVisualization()
		{
			resetTransformLayout(voidZone);
			resetTransformLayout(joystickZone);
			hideVisualization();
		}
		private void resetTransformLayout(RectTransform target)
		{
			if (target == null) return;

			Vector2 halfVector = new Vector2(0.5f, 0.5f);
			target.anchorMax = halfVector;
			target.anchorMin = halfVector;
			target.pivot = halfVector;
		}

		private void showVisualization()
		{
			showTransform(voidZone, startPoint, voidZoneRadius);
			showTransform(joystickZone, startPoint, joystickRadius);
			showTransform(pressZone, pressPoint, pressZoneRadius);
		}
		private void showTransform(RectTransform target, Vector2 position, float radius)
		{
			if (target == null) return;

			target.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, radius * 2);
			target.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, radius * 2);
			target.position = position;
			target.gameObject.SetActive(true);
		}

		private void hideVisualization()
		{
			if (voidZone != null) voidZone.gameObject.SetActive(false);
			if (joystickZone != null) joystickZone.gameObject.SetActive(false);
			if (pressZone != null) pressZone.gameObject.SetActive(false);
		}
	}
}