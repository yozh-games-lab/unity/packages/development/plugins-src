﻿using UnityEngine;
using UnityEditor;

namespace YOZH.Plugins.OcclusionCulling.Editor
{
	[CustomEditor(typeof(OcclusionTree))]
	public class OcclusionTreeEditor : UnityEditor.Editor
	{

		public override void OnInspectorGUI()
		{
			GUI.enabled = !Application.isPlaying;

			DrawDefaultInspector();
			if (!GUI.enabled) return;

			OcclusionTree tree = (OcclusionTree)target;

			SerializedProperty minNodeExtends = serializedObject.FindProperty("minNodeExtends");
			minNodeExtends.vector3Value = clampMinNodeExtends(minNodeExtends.vector3Value);

			SerializedProperty rootNodeBounds = serializedObject.FindProperty("rootNodeBounds");
			rootNodeBounds.boundsValue = clampRootNodeBounds(rootNodeBounds.boundsValue, minNodeExtends.vector3Value);

			tree.GetRootNode().UpdateBounds(rootNodeBounds.boundsValue, minNodeExtends.vector3Value);

			serializedObject.ApplyModifiedProperties();
		}

		private void OnSceneGUI()
		{
			OcclusionTree tree = (OcclusionTree)target;
			drawNode(tree.GetRootNode());
		}

		private void drawNode(OcclusionTreeNode node)
		{
			if (node.ChildCount > 0) {
				OcclusionTreeNode[] children = node.Children;
				foreach (OcclusionTreeNode child in children) {
					drawNode(child);
				}
			}
			else {
				Handles.color = new Color(1, 0, 1, 1);
				Handles.DrawWireCube(node.Bounds.center, node.Bounds.size);
			}
		}

		private Vector3 clampMinNodeExtends(Vector3 vector)
		{
			vector.x = Mathf.Clamp(vector.x, OcclusionTreeNode.MIN_NODE_EXTENDS, float.MaxValue);
			vector.y = Mathf.Clamp(vector.y, OcclusionTreeNode.MIN_NODE_EXTENDS, float.MaxValue);
			vector.z = Mathf.Clamp(vector.z, OcclusionTreeNode.MIN_NODE_EXTENDS, float.MaxValue);
			return vector;
		}

		private Bounds clampRootNodeBounds(Bounds bounds, Vector3 minExtents)
		{
			Vector3 extents = new Vector3(
								  Mathf.Clamp(bounds.extents.x, minExtents.x, float.MaxValue),
								  Mathf.Clamp(bounds.extents.y, minExtents.y, float.MaxValue),
								  Mathf.Clamp(bounds.extents.z, minExtents.z, float.MaxValue)
							  );
			bounds.extents = extents;
			return bounds;
		}
	}
}