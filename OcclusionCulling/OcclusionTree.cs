﻿using UnityEngine;
using System.Collections;


namespace YOZH.Plugins.OcclusionCulling
{
	public class OcclusionTree : MonoBehaviour
	{

		private static OcclusionTree instance = null;
		public static OcclusionTree Instance {
			get {
				return getInstance();
			}
			private set {
				if (instance == value) return;

				if (instance == null) {
					instance = value;
				}
				else {
					DestroyImmediate(value);
				}
			}
		}

		private static OcclusionTree getInstance()
		{
			if (instance == null) {
				instance = FindObjectOfType<OcclusionTree>();
				if (instance == null) {
					instance = new GameObject(typeof(OcclusionTree).Name, typeof(OcclusionTree)).GetComponent<OcclusionTree>();
				}
			}
			return instance;
		}

		public static void RegisterAgent(IOcclusionAgent agent)
		{
			if (!agent.IsStatic) return;

			Instance.getRootNode().AddAgent(agent);
		}

		public static void UnregisterAgent(IOcclusionAgent agent)
		{
			if (instance != null) {
				instance.getRootNode().RemoveAgent(agent);
			}
		}

		private OcclusionTreeNode rootNode = null;
		[SerializeField]
		private Bounds rootNodeBounds = new Bounds(Vector3.zero, new Vector3(128, 128, 128));
		[SerializeField]
		private Vector3 minNodeExtends = new Vector3(8, 8, 8);
		[SerializeField, Range(1, 60)]
		private int updateFrameRate = 1;

		private Camera[] cameras = new Camera[0];

		private void Awake()
		{
			Instance = this;
			cameras = new Camera[] { Camera.main };
		}

		private void OnEnable()
		{
			StartCoroutine(updateState());
		}

		private void OnDisable()
		{
			StopAllCoroutines();
			getRootNode().ForceVisible();
		}

		private IEnumerator updateState()
		{
			while (true) {
				if (Time.frameCount % updateFrameRate == 0) {
					getRootNode().UpdateState(cameras);
				}
				yield return new WaitForEndOfFrame();
			}
		}

		private OcclusionTreeNode getRootNode()
		{
			if (rootNode == null) {
				rootNode = new OcclusionTreeNode(rootNodeBounds, minNodeExtends);
			}
			return rootNode;
		}


#if UNITY_EDITOR
		public OcclusionTreeNode GetRootNode()
		{
			return getRootNode();
		}
#endif
	}
}