﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using YOZH.Core.APIExtentions.Unity;

namespace YOZH.Plugins.OcclusionCulling
{
	public class OcclusionTreeNode
	{
		public const float MIN_NODE_EXTENDS = 0.5f;

		private Bounds bounds;
		public Bounds Bounds { get { return bounds; } }

		private OcclusionTreeNode[] children = new OcclusionTreeNode[0];
		private List<IOcclusionAgent> agents = new List<IOcclusionAgent>();

		public OcclusionTreeNode(Bounds bounds, Vector3 minChildExtents)
		{
			updateBounds(bounds, minChildExtents);
		}

		public void UpdateState(Camera[] cameras)
		{
			if (isVisibleByAnyCamera(cameras, bounds)) {
				foreach (OcclusionTreeNode node in children) {
					node.UpdateState(cameras);
				}
				enableAgents();
			}
			else {
				disableAgentsRecursive();
			}
		}
		public bool AddAgent(IOcclusionAgent agent)
		{
			foreach (OcclusionTreeNode node in children) {
				if (node.AddAgent(agent)) {
					return true;
				}
			}
			if (bounds.Contains(agent.Bounds)) {
				addAgent(agent);
				return true;
			}
			return false;
		}
		public void RemoveAgent(IOcclusionAgent agent)
		{
			removeAgent(agent);
			foreach (OcclusionTreeNode node in children) {
				node.RemoveAgent(agent);
			}
		}
		public void ForceVisible()
		{
			enableAgentsRecursive();
		}
		public void ForceInvisible()
		{
			disableAgentsRecursive();
		}

		private bool isVisibleByAnyCamera(Camera[] cameras, Bounds bounds)
		{
			foreach (Camera camera in cameras) {
				if (camera.SeesBounds(bounds)) {
					return true;
				}
			}
			return false;
		}
		private void updateBounds(Bounds bounds, Vector3 minChildExtents)
		{
			this.bounds = correctedBounds(bounds);
			updateChildrenList(minChildExtents);
		}
		private void updateChildrenList(Vector3 minChildExtents)
		{
			children = new OcclusionTreeNode[0];
			minChildExtents = correctedMinChildExtends(minChildExtents);

			if (bounds.extents.x < minChildExtents.x && bounds.extents.y < minChildExtents.y && bounds.extents.z < minChildExtents.z) {
				return;
			}

			if (bounds.extents.x >= minChildExtents.x * 2) {
				Vector3 childSize = new Vector3(bounds.extents.x, bounds.size.y, bounds.size.z);
				Vector3 childCenter1 = bounds.center + new Vector3(bounds.extents.x / 2, 0, 0);
				Vector3 childCenter2 = bounds.center - new Vector3(bounds.extents.x / 2, 0, 0);
				children = new OcclusionTreeNode[]
				{
					new OcclusionTreeNode(new Bounds(childCenter1, childSize), minChildExtents),
					new OcclusionTreeNode(new Bounds(childCenter2, childSize), minChildExtents)
				};
				return;
			}

			if (bounds.extents.y >= minChildExtents.y * 2) {
				Vector3 childSize = new Vector3(bounds.size.x, bounds.extents.y, bounds.size.z);
				Vector3 childCenter1 = bounds.center + new Vector3(0, bounds.extents.y / 2, 0);
				Vector3 childCenter2 = bounds.center - new Vector3(0, bounds.extents.y / 2, 0);
				children = new OcclusionTreeNode[]
				{
					new OcclusionTreeNode(new Bounds(childCenter1, childSize), minChildExtents),
					new OcclusionTreeNode(new Bounds(childCenter2, childSize), minChildExtents)
				};
				return;
			}

			if (bounds.extents.z >= minChildExtents.z * 2) {
				Vector3 childSize = new Vector3(bounds.size.x, bounds.size.y, bounds.extents.z);
				Vector3 childCenter1 = bounds.center + new Vector3(0, 0, bounds.extents.z / 2);
				Vector3 childCenter2 = bounds.center - new Vector3(0, 0, bounds.extents.z / 2);
				children = new OcclusionTreeNode[]
				{
					new OcclusionTreeNode(new Bounds(childCenter1, childSize), minChildExtents),
					new OcclusionTreeNode(new Bounds(childCenter2, childSize), minChildExtents)
				};
				return;
			}
		}
		private void addAgent(IOcclusionAgent agent)
		{
			if (!agents.Contains(agent)) {
				agents.Add(agent);
			}
		}
		private void removeAgent(IOcclusionAgent agent)
		{
			agents.Remove(agent);
		}
		private void enableAgents()
		{
			foreach (IOcclusionAgent agent in agents) {
				if (!agent.IsVisible) {
					agent.SetVisible();
				}
			}
		}
		private void disableAgents()
		{
			foreach (IOcclusionAgent agent in agents) {
				if (agent.IsVisible) {
					agent.SetInvisible();
				}
			}
		}
		private void disableAgentsRecursive()
		{
			disableAgents();
			foreach (OcclusionTreeNode node in children) {
				node.disableAgentsRecursive();
			}
		}
		private void enableAgentsRecursive()
		{
			enableAgents();
			foreach (OcclusionTreeNode node in children) {
				node.enableAgentsRecursive();
			}
		}
		private Bounds correctedBounds(Bounds bounds)
		{
			bounds.extents = new Vector3(
				Mathf.Clamp(bounds.extents.x, MIN_NODE_EXTENDS, float.MaxValue),
				Mathf.Clamp(bounds.extents.y, MIN_NODE_EXTENDS, float.MaxValue),
				Mathf.Clamp(bounds.extents.z, MIN_NODE_EXTENDS, float.MaxValue)
			);
			return bounds;
		}
		private Vector3 correctedMinChildExtends(Vector3 minChildExtents)
		{
			minChildExtents.x = Mathf.Clamp(minChildExtents.x, MIN_NODE_EXTENDS, float.MaxValue);
			minChildExtents.y = Mathf.Clamp(minChildExtents.y, MIN_NODE_EXTENDS, float.MaxValue);
			minChildExtents.z = Mathf.Clamp(minChildExtents.z, MIN_NODE_EXTENDS, float.MaxValue);
			return minChildExtents;
		}


#if UNITY_EDITOR
		public void UpdateBounds(Bounds bounds, Vector3 minChildExtents)
		{
			updateBounds(bounds, minChildExtents);
		}
		public int ChildCount { get { return children.Length; } }
		public OcclusionTreeNode[] Children {
			get {
				OcclusionTreeNode[] result = new OcclusionTreeNode[children.Length];
				children.CopyTo(result, 0);
				return result;
			}
		}


		public void DrawGizmos()
		{
			if (children.Length == 0) {
				Gizmos.color = new Color(1, 0, 1, 1);
				Gizmos.DrawWireCube(bounds.center, bounds.size);
			}
			else {
				foreach (OcclusionTreeNode child in children) {
					child.DrawGizmos();
				}
			}
		}
#endif
	}
}