﻿using UnityEngine;
using System.Collections;

namespace YOZH.Plugins.OcclusionCulling
{
	[RequireComponent(typeof(Animator))]
	public class AnimatorOcclusionAgent : MonoBehaviour, IOcclusionAgent
	{
		[SerializeField]
		private Vector3 boundsSize = Vector3.one;

		private Bounds thisBounds;
		private Animator thisAnimator = null;

		private void Awake()
		{
			thisAnimator = GetComponent<Animator>();
			thisBounds = new Bounds(transform.position, boundsSize);
			OcclusionTree.RegisterAgent(this);
		}

		private void OnDestroy()
		{
			OcclusionTree.UnregisterAgent(this);
		}

		#region IOcclusionAgent implementation

		public void SetVisible()
		{
			thisAnimator.enabled = true;
		}

		public void SetInvisible()
		{
			thisAnimator.enabled = false;
		}

		public Bounds Bounds { get { return thisBounds; } }
		public bool IsVisible { get { return thisAnimator.enabled; } }
		public bool IsStatic { get { return gameObject.isStatic; } }

		#endregion

	}
}