﻿using UnityEngine;
using System.Collections;


namespace YOZH.Plugins.OcclusionCulling
{
	public interface IOcclusionAgent
	{
		Bounds Bounds { get; }
		bool IsVisible { get; }
		bool IsStatic { get; }
		void SetVisible();
		void SetInvisible();
	}
}