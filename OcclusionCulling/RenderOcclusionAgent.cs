﻿using UnityEngine;
using System.Collections;


namespace YOZH.Plugins.OcclusionCulling
{
	[RequireComponent(typeof(Renderer))]
	public class RenderOcclusionAgent : MonoBehaviour, IOcclusionAgent
	{
		private Renderer thisRenderer = null;

		private void Awake()
		{
			thisRenderer = GetComponent<Renderer>();
			OcclusionTree.RegisterAgent(this);
		}

		private void OnDestroy()
		{
			OcclusionTree.UnregisterAgent(this);
		}

		#region IOcclusionAgent implementation
		public Bounds Bounds {
			get {
				return thisRenderer.bounds;
			}
		}

		public bool IsVisible {
			get {
				return thisRenderer.enabled;
			}
		}

		public bool IsStatic {
			get {
				return gameObject.isStatic;
			}
		}

		public void SetVisible()
		{
			thisRenderer.enabled = true;
		}

		public void SetInvisible()
		{
			thisRenderer.enabled = false;
		}
		#endregion


	}
}