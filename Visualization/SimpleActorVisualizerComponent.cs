﻿using System;
using System.Collections;
using UnityEngine;

namespace YOZH.Plugins.Visualization
{
	public class SimpleActorVisualizerComponent : MonoBehaviour, IActorVisualizerComponent
	{
		[SerializeField]
		private SimpleActorVisualizer enterVisualization = new SimpleActorVisualizer();
		[SerializeField]
		private SimpleActorVisualizer exitVisualization = new SimpleActorVisualizer();

		[SerializeField]
		private Animation thisAnimation = null;

		public GameObject GameObject {
			get {
				return gameObject;
			}
		}

		public void Break()
		{
			StopCoroutine("waitForExitVisualization");
			enterVisualization.Break(thisAnimation);
			exitVisualization.Execute(thisAnimation);
		}

		public void Execute()
		{
			if (!IsFinished()) return;

			enterVisualization.Execute(thisAnimation);
			StartCoroutine("waitForExitVisualization");
		}

		public T FindComponent<T>() where T : Component
		{
			return gameObject.GetComponent<T>();
		}

		public bool IsFinished()
		{
			return enterVisualization.IsFinished(thisAnimation) && exitVisualization.IsFinished(thisAnimation);
		}

		public bool IsFinishedAnimation()
		{
			return enterVisualization.IsFinishedAnimation(thisAnimation) && exitVisualization.IsFinishedAnimation(thisAnimation);
		}

		public bool IsFinishedParticles()
		{
			return enterVisualization.IsFinishedParticles() && exitVisualization.IsFinishedParticles();
		}

		public bool IsFinishedSound()
		{
			return enterVisualization.IsFinishedSound() && exitVisualization.IsFinishedSound();
		}

		private IEnumerator waitForExitVisualization()
		{
			while (!enterVisualization.IsFinished(thisAnimation)) {
				yield return new WaitForEndOfFrame();
			}
			exitVisualization.Execute(thisAnimation);
		}
	}
}
