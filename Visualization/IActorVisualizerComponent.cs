﻿using YOZH.Core.Misc.Interfaces;

namespace YOZH.Plugins.Visualization
{
	public interface IActorVisualizerComponent : IGameObjectComponent
	{
		void Execute();
		void Break();
		bool IsFinished();
		bool IsFinishedAnimation();
		bool IsFinishedSound();
		bool IsFinishedParticles();
	}
}
