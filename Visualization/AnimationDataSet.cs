﻿using System;
using UnityEngine;


namespace YOZH.Plugins.Visualization
{
	[Serializable]
	public class AnimationDataSet
	{
		[SerializeField]
		private AnimationClip clip = null;

		[SerializeField]
		private PlayMode playMode = PlayMode.StopSameLayer;

		public bool IsFinished(Animation anim)
		{
			if (!isValid(anim)) return true;

			return !anim.isPlaying;
		}
		public void Execute(Animation anim)
		{
			if (!isValid(anim)) return;

			anim.AddClip(clip, clip.name);
			anim.clip = clip;
			anim.Play(playMode);
		}
		public void Break(Animation anim)
		{
			if (!isValid(anim)) return;

			anim.Stop();
		}

		private bool isValid(Animation anim)
		{
			return anim != null && clip != null && clip.legacy;
		}
	}
}
