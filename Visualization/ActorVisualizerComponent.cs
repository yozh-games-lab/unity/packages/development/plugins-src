﻿using UnityEngine;

namespace YOZH.Plugins.Visualization
{
	public class ActorVisualizerComponent : MonoBehaviour, IActorVisualizerComponent
	{
		[SerializeField]
		private ActorVisualizer visualization = new ActorVisualizer();
		[SerializeField]
		private Animator animator = null;

		public GameObject GameObject {
			get {
				return gameObject;
			}
		}

		public void Break()
		{
			visualization.Break(animator);
		}

		public void Execute()
		{
			visualization.Execute(animator);
		}

		public T FindComponent<T>() where T : Component
		{
			return gameObject.GetComponent<T>();
		}

		public bool IsFinished()
		{
			return visualization.IsFinished(animator);
		}

		public bool IsFinishedAnimation()
		{
			return visualization.IsFinishedAnimation(animator);
		}

		public bool IsFinishedParticles()
		{
			return visualization.IsFinishedParticles();
		}

		public bool IsFinishedSound()
		{
			return visualization.IsFinishedSound();
		}
	}
}
