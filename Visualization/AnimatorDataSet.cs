﻿using System;
using UnityEngine;


namespace YOZH.Plugins.Visualization
{
	[Serializable]
	public class AnimatorDataSet
	{
		[SerializeField]
		private string parameterName = "";

		[SerializeField]
		private bool booleanValue = false;
		[SerializeField]
		private bool isTrigger = true;

		public bool IsFinished(Animator controller)
		{
			if (!isValid(controller)) return true;

			if (isTrigger) {
				return controller.GetBool(parameterName) == false;
			}
			else {
				return controller.GetBool(parameterName) == booleanValue;
			}
		}
		public void Execute(Animator controller)
		{
			if (!isValid(controller)) return;

			if (isTrigger) {
				controller.SetTrigger(parameterName);
			}
			else {
				controller.SetBool(parameterName, booleanValue);
			}
		}
		public void Break(Animator controller)
		{
			if (!isValid(controller)) return;

			if (isTrigger) {
				controller.ResetTrigger(parameterName);
			}
			else {
				controller.SetBool(parameterName, !booleanValue);
			}
		}

		private bool isValid(Animator controller)
		{
			return controller != null && controller.isActiveAndEnabled && !string.IsNullOrEmpty(parameterName);
		}
	}
}