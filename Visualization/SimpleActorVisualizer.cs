﻿using UnityEngine;
using System;
using YOZH.Plugins.SoundManager;


namespace YOZH.Plugins.Visualization
{
	[Serializable]
	public class SimpleActorVisualizer
	{
		[SerializeField]
		private AnimationDataSet animation = new AnimationDataSet();

		[SerializeField]
		private SoundEvent sound = new SoundEvent();

		[SerializeField]
		private ParticleSystem particleSystem = null;

		public void Execute(Animation anim)
		{
			animation.Execute(anim);
			sound.Execute();
			executeParticles();
		}
		public void Break(Animation anim)
		{
			animation.Break(anim);
			sound.Break();
			breakParticles();
		}
		public bool IsFinished(Animation anim)
		{
			return IsFinishedAnimation(anim) && IsFinishedParticles() && IsFinishedSound();
		}
		public bool IsFinishedAnimation(Animation anim)
		{
			return animation.IsFinished(anim);
		}
		public bool IsFinishedSound()
		{
			return sound.IsFinished();
		}
		public bool IsFinishedParticles()
		{
			return particleSystem == null || !particleSystem.IsAlive();
		}

		private void executeParticles()
		{
			if (particleSystem != null) {
				particleSystem.Play(true);
			}
		}
		private void breakParticles()
		{
			if (particleSystem != null) {
				particleSystem.Stop(true);
			}
		}
	}
}
