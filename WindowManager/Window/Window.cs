﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;


namespace YOZH.Plugins.WindowManager
{
	public partial class Window : MonoBehaviour
	{

		#region Events
		protected void Awake()
		{
			raiseOnLoad(this);

			if (awakeAsRoot) doAwakeAsRoot();
			onAwakeWindow();
		}
		protected virtual void onDestroy()
		{
			if (IsRoot && workingCanvas != null) {
				RootsCollection.Remove(workingCanvas);
			}
			onDestroyObject();
		}
		protected virtual void onAwakeWindow()
		{
		}
		protected virtual void onDestroyObject()
		{
		}
		protected virtual void onBeforeOpen()
		{
		}
		protected virtual void onBeforeClose()
		{
		}
		protected virtual void onAfterOpen()
		{
		}
		protected virtual void onAfterClose()
		{
		}
		protected virtual void onLoad()
		{
		}
		protected virtual void onDestroyWin()
		{
		}
		protected virtual void onSelect()
		{
		}
		protected virtual void onBeforeShow()
		{
		}
		protected virtual void onAfterShow()
		{
		}
		protected virtual void onBeforeHide()
		{
		}
		protected virtual void onAfterHide()
		{
		}

		public event StateChangedDelegate OnCurrentStateChanged;
		private void raiseCurrentStateChanged(Window sender, State newWinState)
		{
			if (OnCurrentStateChanged != null) OnCurrentStateChanged(sender, newWinState);
		}
		#endregion

		#region Interface
		#region Properties
		public State CurrentState {
			get { return currentState; }
			protected set {
				if (currentState != value) {
					State prevState = currentState;
					currentState = value;
					switch (currentState) {
						case State.Closing:
							raiseOnBeforeClose(this);
							break;
						case State.Closed:
							raiseOnAfterClose(this);
							break;
						case State.Hiding:
							raiseOnBeforeHide(this);
							break;
						case State.Hidden:
							raiseOnAfterHide(this);
							break;
						case State.Opening:
							raiseOnBeforeOpen(this);
							break;
						case State.Showing:
							raiseOnBeforeShow(this);
							break;
						case State.OpenedOrShown:
							switch (prevState) {
								case State.Opening:
									raiseOnAfterOpen(this);
									break;
								case State.Showing:
									raiseOnAfterShow(this);
									break;
							}
							break;
					}
					raiseCurrentStateChanged(this, value);
				}
			}
		}
		public bool IsVisible {
			get { return isVisible; }
		}
		public bool IsRoot {
			get { return RootsCollection.ContainsWindow(this); }
		}
		public bool IsSelectedRoot { get { return CurrentRoot == this; } }
		public bool IsReserved { get { return ParentRoot.reservedWindows.Contains(this); } }
		public bool IsSelected { get { return SelectedWindow == this; } }
		public Window ParentRoot {
			get {
				if (IsRoot) return this;

				Window parent = null;
				if (workingCanvas != null) RootsCollection.FindWindow(workingCanvas, out parent);
				return parent;
			}
		}
		public Window ParentWindow { get { return parentWindow; } }
		public List<Window> ChildWindows { get { return new List<Window>(childWindows); } }
		public int ChildWindowsCount { get { return childWindows.Count; } }
		public bool DropOnClose = false;
		public Canvas WorkingCanvas { get { return workingCanvas; } }
		public string LoadedPath { get { return loadedPath; } }
		public bool AutoSelect { get { return autoSelect; } }
		#endregion

		#region Public Methods
		public Window GetWindow(string path)
		{
			return getWindow(path);
		}
		public T GetWindow<T>(string path) where T : Window
		{
			return getWindow(path) as T;
		}
		public void ReturnWindow()
		{
			returnWindow();
		}
		public void Open()
		{
			openThis();
		}
		public void OpenChild(string path)
		{
			OpenChild<Window>(path);
		}
		public T OpenChild<T>(string path) where T : Window
		{
			return openChild(path) as T;
		}
		public void Close()
		{
			closeThisRecursive();
		}
		public void Select()
		{
			selectThis();
		}
		public void DestroyWin()
		{
			destroyWin();
		}
		public void Show()
		{
			showThis();
		}
		public void Hide()
		{
			hideThis();
		}
		public void HideAndOpenChild(string path)
		{
			Window child = getWindow(path);
			HideAndOpenChild(child);
		}
		public void HideAndOpenChild(Window child)
		{
			if (child.parentWindow != this) return;

			ParentRoot.StartCoroutine(hideAndOpenRoutine(child));
		}
		public void ForceState(State state)
		{
			forceState(state);
		}
		#endregion
		#endregion

		#region Back-end
		#region Variables
		private bool isLoaded = false;
		[SerializeField, HideInInspector]
		private bool awakeAsRoot = false;
		[SerializeField, HideInInspector]
		private bool autoSelect = true;
		private State currentState = State.Closed;
		private bool isVisible {
			get { return gameObject.activeSelf; }
			set { gameObject.SetActive(value); }
		}
		private List<Window> reservedWindows = new List<Window>();
		private Window parentWindow = null;
		private List<Window> childWindows = new List<Window>();
		private Canvas workingCanvas { get { return transform.parent.GetComponentInParent<Canvas>(); } }
		private List<Window> loadedWins { get { return new List<Window>(ParentRoot.transform.GetComponentsInChildren<Window>(true)); } }
		private string loadedPath = "";

		#region Coroutines
		private IEnumerator switchStateRoutine = null;
		#endregion

		#endregion

		#region Private Methods
		private static Canvas getDefaultCanvas()
		{

			RootsCollection.ClearNullCanvasesAndWindows();

			if (RootsCollection.Count > 0) {
				return CanvasesList[0];
			}
			else {
				Canvas[] canvasesOnScene = FindObjectsOfType<Canvas>();
				if (canvasesOnScene.Length > 0) {
					foreach (Canvas canvas in canvasesOnScene) {
						if (canvas.gameObject.activeInHierarchy) {
							return canvas;
						}
					}
				}

				Canvas canvasSample = Resources.Load<Canvas>(PATH_TO_DEFAULT_CANVAS);
				if (canvasSample != null) {
					Canvas result = Instantiate(canvasSample);
					result.name = result.name.Replace("(Clone)", "");
					return result;
				}
				else return null;
			}
		}
		private void addChildWindow(Window window)
		{
			if (window != null && window != this && !childWindows.Contains(window)) childWindows.Add(window);
		}
		private void removeChildWindow(Window window)
		{
			if (window != null && childWindows.Contains(window)) childWindows.Remove(window);
		}
		public void setParent(Window parent)
		{
			if (IsRoot) return;

			if (parentWindow != null) parentWindow.removeChildWindow(this);

			if (parent != null) parent.addChildWindow(this);

			parentWindow = parent;
		}

		private Window loadWindow(string path)
		{

			path = Core.Misc.MiscUtilities.NormalizePath(path);
			string fullPath = string.Format(@"{0}/{1}", PATH_TO_WINDOWS_PREFABS, path);
			Window source = Resources.Load<Window>(fullPath);
			Window instance = null;

			if (source != null) {
				instance = Instantiate(source);
				if (instance != null) {
					instance.name = instance.name.Replace(@"(Clone)", @"");
					instance.transform.SetParent(ParentRoot.transform, false);
					instance.gameObject.SetActive(false);
					instance.loadedPath = path;
				}
			}
			else {
				throw new FileNotFoundException(fullPath);
			}

			raiseOnLoad(this);
			return instance;
		}
		private bool findFreeWindow(string name, out Window result)
		{
			foreach (Window w in loadedWins) {
				if (!w.IsReserved && w.name == name && !w.IsRoot) {
					result = w;
					return true;
				}
			}
			result = null;
			return false;
		}
		private Window getFreeWindow(string path)
		{
			Window result = null;

			if (!findFreeWindow(pathToName(path), out result)) result = loadWindow(path);

			return result;
		}
		private Window getWindow(string path)
		{
			Window target = getFreeWindow(path);
			if (target != null) {
				reserve(target);
			}
			return target;
		}
		private void returnWindow()
		{
			if (currentState != State.Closed || currentState != State.Closing) closeThisRecursive();
			else release(this);
		}

		private Window openChild(string path)
		{
			Window target = getFreeWindow(path);
			if (target != null) {
				reserve(target);
				target.openThis();
			}
			return target;
		}
		protected void openThis()
		{
			if (currentState != State.Closed && currentState != State.Closing) return;

			if (switchStateRoutine != null) StopCoroutine(switchStateRoutine);
			isVisible = true;
			if (autoSelect) selectThis();

			switchStateRoutine = openRoutine();
			StartCoroutine(switchStateRoutine);
		}

		protected void closeThis()
		{
			switch (currentState) {
				case State.Opening:
				case State.OpenedOrShown:
					if (switchStateRoutine != null) StopCoroutine(switchStateRoutine);
					switchStateRoutine = closeRoutine(onEndCloseThis);
					StartCoroutine(switchStateRoutine);
					break;

				case State.Hiding:
				case State.Hidden:
					CurrentState = State.Closing;
					CurrentState = State.Closed;
					break;
			}
		}
		private void closeThisRecursive()
		{
			if (IsRoot) RemoveRoot(workingCanvas);
			else {
				for (int i = childWindows.Count - 1; i >= 0; i--) childWindows[i].closeThisRecursive();

				closeThis();
			}
		}
		private void onEndCloseThis()
		{
			selectBack();
			release(this);

			if (DropOnClose) destroyWin();
			else isVisible = false;
		}

		private IEnumerator openRoutine(Action onEnd = null)
		{
			CurrentState = State.Opening;

			yield return StartCoroutine(waitForOpenEffect());

			if (onEnd != null) onEnd();
			CurrentState = State.OpenedOrShown;

			switchStateRoutine = null;
			yield break;
		}
		private IEnumerator closeRoutine(Action onEnd = null)
		{
			CurrentState = State.Closing;

			yield return StartCoroutine(waitForCloseEffect());

			if (onEnd != null) onEnd();
			CurrentState = State.Closed;

			switchStateRoutine = null;
			yield break;
		}

		protected virtual IEnumerator waitForOpenEffect()
		{
			yield break;
		}
		protected virtual IEnumerator waitForCloseEffect()
		{
			yield break;
		}

		private void selectThis()
		{
			SelectedWindow = this;
			transform.SetAsLastSibling();
			if (IsRoot) SelectRoot(workingCanvas);
			if (CurrentState == State.Hidden) showThis();
			raiseOnSelect(this);
		}
		private void selectBack()
		{
			if (this == null || !IsSelected || (!IsRoot && parentWindow == null)) return;

			if (IsRoot) {
				RootsCollection.SelectLastAddedWindow();
			}
			else {
				parentWindow.selectLastChild();
			}
		}
		private void selectLastChild()
		{
			if (childWindows.Count > 0) {
				for (int i = childWindows.Count - 1; i >= 0; i--) {
					if (childWindows[i] != SelectedWindow) {
						childWindows[i].selectThis();
						return;
					}
				}
			}
			selectThis();
		}

		private void destroyWin()
		{
			if (!IsRoot) closeThisRecursive();

			raiseOnDestroyWin(this);
			Destroy(gameObject);
		}
		private void reserve(Window window)
		{
			if (window != null && !window.IsReserved) {
				ParentRoot.reservedWindows.Add(window);
				window.setParent(this);
			}
		}
		private void release(Window window)
		{
			if (window != null && window.IsReserved) {
				ParentRoot.reservedWindows.Remove(window);
				window.setParent(null);
			}
		}

		protected void showThis()
		{
			if (currentState != State.Hidden) return;

			if (switchStateRoutine != null) StopCoroutine(switchStateRoutine);
			isVisible = true;
			switchStateRoutine = showRoutine();
			StartCoroutine(switchStateRoutine);
			if (autoSelect) selectThis();
		}
		protected void hideThis()
		{
			if (currentState != State.OpenedOrShown) return;

			if (switchStateRoutine != null) StopCoroutine(switchStateRoutine);
			switchStateRoutine = hideRoutine(onEndHideThis);
			StartCoroutine(switchStateRoutine);
		}
		private void onEndHideThis()
		{
			isVisible = false;
		}

		private IEnumerator showRoutine(Action onEnd = null)
		{
			CurrentState = State.Showing;

			yield return StartCoroutine(waitForShowEffect());

			if (onEnd != null) onEnd();
			CurrentState = State.OpenedOrShown;

			switchStateRoutine = null;
			yield break;
		}
		private IEnumerator hideRoutine(Action onEnd = null)
		{
			CurrentState = State.Hiding;

			yield return StartCoroutine(waitForHideEffect());

			if (onEnd != null) onEnd();
			CurrentState = State.Hidden;

			switchStateRoutine = null;
			yield break;
		}

		protected virtual IEnumerator waitForShowEffect()
		{
			yield break;
		}
		protected virtual IEnumerator waitForHideEffect()
		{
			yield break;
		}

		private IEnumerator hideAndOpenRoutine(Window child)
		{

			if (child == null) yield break;

			hideThis();

			while (CurrentState != State.Hidden) yield return new WaitForEndOfFrame();

			child.Open();
			yield break;
		}
		private void forceState(State state)
		{
			switch (state) {
				case State.Closed:
					raiseOnBeforeClose(this);
					setParent(null);
					isVisible = false;
					CurrentState = state;
					break;

				case State.Hidden:
					raiseOnBeforeHide(this);
					isVisible = false;
					CurrentState = state;
					break;

				case State.OpenedOrShown:
					if (currentState == State.Hidden || currentState == State.Hiding) raiseOnBeforeShow(this);
					if (currentState == State.Closed || currentState == State.Opening) raiseOnBeforeOpen(this);

					isVisible = true;
					CurrentState = state;
					break;
			}
		}
		private string pathToName(string path)
		{
			string[] pathToArr = path.Trim(@" /\".ToCharArray()).Split(@"/\".ToCharArray());
			return pathToArr[pathToArr.Length - 1];
		}
		protected void stretchToParent(RectTransform tr)
		{
			tr.anchorMin = Vector2.zero;
			tr.anchorMax = Vector2.one;
			tr.offsetMin = Vector2.zero;
			tr.offsetMax = Vector2.zero;
			tr.pivot = new Vector2(0.5f, 0.5f);
		}
		protected void doAwakeAsRoot()
		{
			if (workingCanvas != null && !RootsCollection.ContainsCanvas(workingCanvas)) {
				RectTransform rt = GetComponent<RectTransform>();

				stretchToParent(rt);
				CurrentState = State.OpenedOrShown;

				RootsCollection.Add(workingCanvas, this);
				if (autoSelect) SelectRoot(workingCanvas);

				loadedPath = name;
				foreach (Window win in loadedWins) {
					win.loadedPath = win.name;
				}
			}
		}
		#endregion
		#endregion
	}
}
