﻿using UnityEngine;
using System.Collections.Generic;
using System;


namespace YOZH.Plugins.WindowManager
{
	public partial class Window : MonoBehaviour
	{
		#region Statics
		public const string PATH_TO_WINDOWS_PREFABS = "Prefabs/UI/Windows";
		private const string PATH_TO_DEFAULT_CANVAS = "DefaultWinCanvas";

		public static Window CurrentRoot {
			get {
				if (RootsCollection.Current == null) SelectRoot(getDefaultCanvas());
				return RootsCollection.Current;
			}
			private set {
				RootsCollection.Current = value;
			}
		}
		public static Window SelectedWindow { get; private set; }
		public static void SelectRoot(Canvas canvas)
		{
			if (canvas != null) {
				Window newRoot = null;
				if (RootsCollection.FindWindow(canvas, out newRoot)) CurrentRoot = newRoot;
				else CurrentRoot = AddRoot(canvas);
				if (!CurrentRoot.IsSelected) CurrentRoot.selectThis();
			}
			else CurrentRoot = null;
		}
		public static T SelectRoot<T>(Canvas canvas) where T : Window
		{
			SelectRoot(canvas);
			return CurrentRoot as T;
		}
		public static Window AddRoot(Canvas canvas)
		{
			if (canvas == null) return null;

			Window result = null;
			if (!RootsCollection.FindWindow(canvas, out result)) {
				GameObject resultGO = new GameObject(string.Format("RootWin_{0}", RootsCollection.Count));
				RectTransform resultTransform = resultGO.AddComponent<RectTransform>();
				resultTransform.SetParent(canvas.transform, false);

				result = resultTransform.gameObject.AddComponent<Window>();
				result.stretchToParent(resultTransform);
				result.CurrentState = State.OpenedOrShown;
				RootsCollection.Add(canvas, result);
			}
			if (result.autoSelect) SelectRoot(canvas);
			return result;
		}
		public static void RemoveRoot(Canvas canvas)
		{
			if (canvas == null) return;

			Window target = null;
			if (RootsCollection.FindWindow(canvas, out target)) {
				target.destroyWin();
			}
		}
		public static Window[] RootsList {
			get { return RootsCollection.WindowsList; }
		}
		public static T[] RootsListOfType<T>() where T : Window
		{
			return RootsCollection.WindowsListOfType<T>();
		}
		public static Canvas[] CanvasesList {
			get { return RootsCollection.CanvasesList; }
		}

		public delegate void WindowEvent(Window win);
		public static event WindowEvent OnLoad = null;
		public static event WindowEvent OnDestroyWin = null;
		public static event WindowEvent OnBeforeOpen = null;
		public static event WindowEvent OnBeforeClose = null;
		public static event WindowEvent OnAfterOpen = null;
		public static event WindowEvent OnAfterClose = null;
		public static event WindowEvent OnSelect = null;
		public static event WindowEvent OnBeforeShow = null;
		public static event WindowEvent OnAfterShow = null;
		public static event WindowEvent OnBeforeHide = null;
		public static event WindowEvent OnAfterHide = null;

		private static void raiseOnLoad(Window win)
		{
			if (win == null || win.isLoaded) return;
			win.onLoad();
			if (OnLoad != null) OnLoad(win);
		}
		private static void raiseOnDestroyWin(Window win)
		{
			if (win == null) return;
			win.onDestroyWin();
			if (OnDestroyWin != null) OnDestroyWin(win);
		}
		private static void raiseOnBeforeOpen(Window win)
		{
			if (win == null) return;
			win.onBeforeOpen();
			if (OnBeforeOpen != null) OnBeforeOpen(win);
		}
		private static void raiseOnBeforeClose(Window win)
		{
			if (win == null) return;
			win.onBeforeClose();
			if (OnBeforeClose != null) OnBeforeClose(win);
		}
		private static void raiseOnAfterOpen(Window win)
		{
			if (win == null) return;
			win.onAfterOpen();
			if (OnAfterOpen != null) OnAfterOpen(win);
		}
		private static void raiseOnAfterClose(Window win)
		{
			if (win == null) return;
			win.onAfterClose();
			if (OnAfterClose != null) OnAfterClose(win);
		}
		private static void raiseOnSelect(Window win)
		{
			if (win == null) return;
			win.onSelect();
			if (OnSelect != null) OnSelect(win);
		}
		private static void raiseOnBeforeShow(Window win)
		{
			if (win == null) return;
			win.onBeforeShow();
			if (OnBeforeShow != null) OnBeforeShow(win);

		}
		private static void raiseOnAfterShow(Window win)
		{
			if (win == null) return;
			win.onAfterShow();
			if (OnAfterShow != null) OnAfterShow(win);
		}
		private static void raiseOnBeforeHide(Window win)
		{
			if (win == null) return;
			win.onBeforeHide();
			if (OnBeforeHide != null) OnBeforeHide(win);
		}
		private static void raiseOnAfterHide(Window win)
		{
			if (win == null) return;
			win.onAfterHide();
			if (OnAfterHide != null) OnAfterHide(win);
		}
		#endregion

		#region Inner types
		protected static class RootsCollection
		{
			private static List<KeyValuePair<Canvas, Window>> roots = new List<KeyValuePair<Canvas, Window>>();
			public static int Count { get { return roots.Count; } }
			public static Window Current = null;

			public static void Add(Canvas canvas, Window window)
			{
				if (canvas == null) throw new ArgumentNullException("canvas");
				if (window == null) throw new ArgumentNullException("window");

				if (!ContainsCanvas(canvas)) {
					roots.Add(new KeyValuePair<Canvas, Window>(canvas, window));
				}
			}
			public static void Remove(Canvas canvas)
			{
				KeyValuePair<Canvas, Window> target = roots.Find(item => item.Key == canvas);
				roots.Remove(target);
			}
			public static bool ContainsCanvas(Canvas canvas)
			{
				foreach (KeyValuePair<Canvas, Window> item in roots) {
					if (item.Key == canvas) return true;
				}
				return false;
			}
			public static bool ContainsWindow(Window window)
			{
				foreach (KeyValuePair<Canvas, Window> item in roots) {
					if (item.Value == window) return true;
				}
				return false;
			}
			public static bool FindWindow(Canvas key, out Window value)
			{
				foreach (KeyValuePair<Canvas, Window> item in roots) {
					if (item.Key == key) {
						value = item.Value;
						return true;
					}
				}
				value = null;
				return false;
			}
			public static Window[] WindowsList {
				get {

					Window[] result = new Window[roots.Count];

					for (int i = 0; i < roots.Count; i++) {
						result[i] = roots[i].Value;
					}
					return result;
				}
			}
			public static T[] WindowsListOfType<T>() where T : Window
			{
				List<T> result = new List<T>();
				foreach (KeyValuePair<Canvas, Window> item in roots) {
					if (item.Value is T) {
						result.Add(item.Value as T);
					}
				}
				return result.ToArray();
			}
			public static Canvas[] CanvasesList {
				get {
					Canvas[] result = new Canvas[roots.Count];

					for (int i = 0; i < roots.Count; i++) {
						result[i] = roots[i].Key;
					}
					return result;
				}
			}
			public static void ClearNullCanvasesAndWindows()
			{
				for (int i = roots.Count - 1; i >= 0; i--) {
					if (roots[i].Key == null || roots[i].Value == null) {
						roots.Remove(roots[i]);
						i--;
					}
				}
			}
			public static void SelectLastAddedWindow()
			{
				if (roots.Count > 0) {
					for (int i = roots.Count - 1; i >= 0; i--) {
						if (roots[i].Value != Window.SelectedWindow) {
							roots[i].Value.Select();
							return;
						}
					}
				}
			}
		}


		public enum State
		{
			Closing,
			Closed,
			Opening,
			OpenedOrShown,
			Showing,
			Hiding,
			Hidden
		}
		public delegate void StateChangedDelegate(Window sender, State newWinState);
		#endregion
	}
}
