﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

namespace YOZH.Plugins.WindowManager
{
	public class DefaultWindow : Window
	{
		[SerializeField]
		private Text titleUIText = null;
		public string TitleText {
			get {
				return (titleUIText != null) ? titleUIText.text : null;
			}
			set {
				if (titleUIText != null) titleUIText.text = value;
			}
		}
		public Font TitleFont {
			get {
				return (titleUIText != null) ? titleUIText.font : null;
			}
			set {
				if (titleUIText != null) titleUIText.font = value;
			}
		}

		[SerializeField]
		private Text messageUIText = null;
		public string MessageText {
			get {
				return (messageUIText != null) ? messageUIText.text : null;
			}
			set {
				if (messageUIText != null) messageUIText.text = value;
			}
		}
		public Font MessageFont {
			get {
				return (messageUIText != null) ? messageUIText.font : null;
			}
			set {
				if (messageUIText != null) messageUIText.font = value;
			}
		}
	}
}
